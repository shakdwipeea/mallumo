extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;
use structopt::StructOpt;

quick_main!(run);

fn run() -> Result<()> {
    //
    // Process command line arguments
    //
    let mut arguments = VXGIArguments::from_args();

    if arguments.files.len() == 0 {
        arguments.files.push("Objects/sponza_area_lights/area_lights.gltf".to_string());
    }

    // Initialize application (window, context, input, ui)
    let mut app = AppBuilder::new()
        .with_dimensions(1920, 1080)
        .with_title("VXGI example")
        .build();
    let mut input = Input::default();

    let mut options = VXGIOptions::from_arguments(&arguments);
    options.set_levels(9);
    options.set_calculate_direct(false);
    options.set_calculate_indirect_diffuse(true);
    options.set_calculate_indirect_specular(false);
    options.set_calculate_ambient_occlusion(false);
    options.set_anisotropic();
    options.set_hdr();
    options.set_cones(9, ConeDistribution::ConcentricRegular);

    let mut app_ui = VXGIOptionsUI::new(app.width, app.height, options);

    app.renderer.set_disable(EnableOption::DepthTest);
    app.renderer.set_disable(EnableOption::FramebufferSRGB);

    // Load scene
    let mut shape_list = ShapeList::from_files(
        arguments.files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    //
    // Initialize modules for rendering
    //

    // - UI
    let mut ui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;

    // - Cameraaw:  rad, pitch:  rad
    let mut camera = FreeCamera::new(
        Point3::new(0.4458406, 0.0109710805, -0.03990702),
        Rad(2.9812438),
        Rad(-0.22222234),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Rad(0.7175409424302455f32),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    )
    .chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // - Sun
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(1.8877113),
                height_angle: Rad(1.129871),
            },
            512,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap_fast(&mut app.renderer, &mut [&mut shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    // Deferred
    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // - VXGI
    //   defaults to Texture Isotropic variant, may be changed to any VXGIModule (hence the trait)
    //   voxelize static part of geometry before render loop
    let voxel_structure = app_ui.gi_options.voxel_structure;
    let anisotropy = app_ui.gi_options.anisotropic();

    let mut vxgi_module: Box<VXGIModule> = match (voxel_structure, anisotropy) {
        (VoxelStructure::VolumeTexture, false) => {
            Box::new(IsotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::VolumeTexture, true) => {
            Box::new(AnisotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::SparseVoxelOctree, false) => {
            Box::new(IsotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
        (VoxelStructure::SparseVoxelOctree, true) => {
            Box::new(AnisotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
        }
    };
    vxgi_module
        .voxelize(&mut app.renderer, &[&shape_list], true)
        .chain_err(|| "Could not voxelize static geometry")?;
    vxgi_module
        .inject_radiance(&mut app.renderer, &sun)
        .chain_err(|| "Could not inject radiance")?;

    let mut show_ui = false;
    'render_loop: loop {
        // Process the events
        for event in app.poll_events() {
            app_ui.process_event(event.clone(), &app.gl_window);

            match event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&event);
                        camera
                            .process_event(&event, &input, 0.0333)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&event);
                    camera
                        .process_event(&event, &input, 0.0333)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    deferred_collector
                        .render(&mut app.renderer, &[&shape_list], &camera)
                        .chain_err(|| "Could not render deferred textures")?;

                    ui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Could not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => {
                    if input.state == ElementState::Pressed {
                        if let Some(keycode) = input.virtual_keycode {
                            match keycode {
                                glutin::VirtualKeyCode::H => {
                                    show_ui = !show_ui;
                                }
                                glutin::VirtualKeyCode::R => {
                                    vxgi_module.reload_shaders();
                                }
                                _ => {}
                            }
                        }
                    }
                }
                Event::WindowEvent {
                    event: WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => (),
            };
        }

        //
        // Update part of loop
        //
        // - Camera
        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        // - Shadowmap if sun's position was changed
        if app_ui.sun_variables.update {
            sun.set_position(SunPosition {
                y_angle: Rad(app_ui.sun_variables.angle_x),
                height_angle: Rad(app_ui.sun_variables.angle_y),
            })
            .chain_err(|| "Could not set Sun's poisition")?;

            sun.render_shadowmap_fast(&mut app.renderer, &mut [&mut shape_list])
                .chain_err(|| "Could not render shadowmap")?;

            app_ui.sun_variables.update = false;
        }

        if app_ui.update_gi_options {
            vxgi_module.update_options(app_ui.gi_options);
            app_ui.update_gi_options = false;
        }

        if app_ui.update_gi_structure {
            let voxel_structure = app_ui.gi_options.voxel_structure;
            let anisotropy = app_ui.gi_options.anisotropic();

            vxgi_module = match (voxel_structure, anisotropy) {
                (VoxelStructure::VolumeTexture, false) => {
                    Box::new(IsotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::VolumeTexture, true) => {
                    Box::new(AnisotropicTextureGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::SparseVoxelOctree, false) => {
                    Box::new(IsotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
                (VoxelStructure::SparseVoxelOctree, true) => {
                    Box::new(AnisotropicSVOGI::new(app_ui.gi_options).chain_err(|| "Could not create VXGI module")?)
                }
            };
            vxgi_module
                .voxelize(&mut app.renderer, &[&shape_list], true)
                .chain_err(|| "Could not voxelize static geometry")?;
            app_ui.update_gi_structure = false;
        }

        let current_time = time::precise_time_ns();
        let current_time_ms = (current_time as f64) / (1_000_000f64);
        let current_time_s = current_time_ms / (1_000f64);

        let em = (current_time_s).sin() as f32 * 100.0 + 100.5;
        let em2 = (current_time_s).cos() as f32 * 100.0 + 100.5;

        let mut i = 0;
        for parameters in &mut shape_list.primitive_parameters {
            if parameters.emission.truncate() != Vector3::new(0.0, 0.0, 0.0) {
                parameters.emission = match i {
                    0 => Vector4::new(em, 0.0, 0.0, 1.0),
                    1 => Vector4::new(0.0, em2, 0.0, 1.0),
                    _ => Vector4::new(0.0, 0.0, em, 1.0),
                };
                i += 1;
            }
        }
        shape_list.update_buffers();

        vxgi_module
            .voxelize(&mut app.renderer, &[&shape_list], true)
            .chain_err(|| "Could not voxelize static geometry")?;

        // - Reinject radiance to voxels (due to changed dynamic geometry)
        // vxgi_module
        //     .inject_radiance(&mut app.renderer, &sun)
        //     .chain_err(|| "Could not inject radiance")?;
        // vxgi_module
        //     .propagate_radiance(&mut app.renderer)
        //     .chain_err(|| "Could not propagate radiance")?;

        // - UI
        app_ui.update_ui();

        //
        // Render part of the loop
        //

        // Clear default framebuffer
        app.renderer.clear_default_framebuffer(ClearBuffers::Color);

        app.renderer
            .set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
        app.renderer.set_linear_blending_factors(
            LinearBlendingFactor::SourceAlpha,
            LinearBlendingFactor::OneMinusSourceAlpha,
            LinearBlendingFactor::SourceAlpha,
            LinearBlendingFactor::OneMinusSourceAlpha,
        );

        deferred_collector
            .render_fast(&mut app.renderer, &mut [&mut shape_list], &camera)
            .chain_err(|| "Could not render deferred textures")?;

        //
        vxgi_module
            .render(&mut app.renderer, &deferred_collector, &camera, &sun)
            .chain_err(|| "Could not create finalrender")?;

        // Render UI as a last step
        if show_ui {
            ui_renderer
                .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
                .chain_err(|| "Could not render GUI")?;
        }

        app.swap_buffers();
    }

    Ok(())
}
