extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::*;
use structopt::StructOpt;

fn average(times: &[u64]) -> u64 {
    times.iter().sum::<u64>() / times.len() as u64
}

quick_main!(run);

fn run() -> Result<()> {
    //
    // Process command line arguments
    //
    let arguments = VXGIArguments::from_args();

    // Initialize application (window, context, input, ui)
    let mut app = AppBuilder::new().with_title("VXGI example").build();

    app.renderer.set_disable(EnableOption::DepthTest);

    // Load scene
    let shape_list = ShapeList::from_files(
        arguments.files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: false,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut timer_query = TimerQuery::new();

    let number_of_runs = 100;

    let levels = 11;

    // loop {
    for levels in 9..=12 {
        let mut options = VXGIOptions::new();
        options.set_levels(levels);
        options.voxel_structure = VoxelStructure::SparseVoxelOctree;

        let options_buffer = MutableBuffer::new(&[options]).chain_err(|| "Could not create options buffer")?;

        let mut voxel_fragment_list_module =
            VoxelFragmentListModule::new(options).chain_err(|| "Could not create voxel fragment list module")?;

        let mut runs = Vec::new();
        let mut count = 0;
        for _ in 0..number_of_runs {
            timer_query.begin();

            count = voxel_fragment_list_module
                .voxelize_count(&mut app.renderer, &options_buffer, &[&shape_list])
                .unwrap();

            runs.push(timer_query.end_ns());
        }

        println!(
            "{}/{}: {}, {:.2}",
            levels,
            2u32.pow(levels),
            count,
            average(&runs) as f64 / 1_000_000f64
        );

        app.swap_buffers();
    }

    Ok(())
}
