extern crate cgmath;
extern crate glutin;
extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain! {}
}

use errors::*;

use cgmath::{Deg, Point3, Rad, Vector3};
use mallumo::glutin::*;
use mallumo::*;

quick_main!(run);

fn run() -> Result<()> {
    let mut app = AppBuilder::new().with_title("Scene example").build();
    let mut input = Input::default();

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });
    app.renderer.set_enable(EnableOption::DepthTest);
    app.renderer.set_enable(EnableOption::CullFace);

    // SCENE
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();
    if files.len() == 0 {
        panic!("usage: gltf-display <PATH>");
    }

    let mut shape_list = ShapeList::from_files(
        files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    println!("Loaded scene.");

    // CAMERA
    let mut camera = SphereCamera::new(
        SpherePosition {
            radius: 1.5,
            y_angle: Rad(0.0),
            height_angle: Deg(45.0).into(),
        },
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, 1.0, 0.0),
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.0001,
        100.0,
    )
    .chain_err(|| "Could not create camera")?;

    // SUN
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(1.8877113),
                height_angle: Rad(1.129871),
            },
            2048,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;
    println!("Ready to render.");

    let mut previous_time = time::precise_time_ns();

    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let delta_time = (current_time - previous_time) as f32 / 1000000000.0;

        sun.render_shadowmap_fast(&mut app.renderer, &mut [&mut shape_list])
            .chain_err(|| "Coult not render sun's shadowmap")?;

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        scene_renderer
            .render_fast(&mut app.renderer, &mut shape_list, &camera, Some(&sun))
            .chain_err(|| "Could not render the scene")?;

        app.swap_buffers();

        for event in app.poll_events() {
            input.process_event(&event);
            camera
                .process_event(&event, &input, delta_time)
                .chain_err(|| "Could not process event in camera")?;

            match event {
                Event::WindowEvent {
                    event: glutin::WindowEvent::Resized(width, height),
                    ..
                } => {
                    if width != 0 && height != 0 {
                        app.renderer.set_viewport(Viewport {
                            x: 0,
                            y: 0,
                            width: width as usize,
                            height: height as usize,
                        });
                    }
                }
                Event::WindowEvent {
                    event: glutin::WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => {}
            }
        }

        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        previous_time = current_time;
    }

    Ok(())
}
