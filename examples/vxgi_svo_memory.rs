extern crate mallumo;
extern crate time;

#[macro_use]
extern crate error_chain;
extern crate structopt;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::*;
use structopt::StructOpt;

fn average(times: &[u64]) -> u64 {
    times.iter().sum::<u64>() / times.len() as u64
}

quick_main!(run);

fn run() -> Result<()> {
    //
    // Process command line arguments
    //
    let arguments = VXGIArguments::from_args();

    // Initialize application (window, context, input, ui)
    let mut app = AppBuilder::new().with_title("VXGI example").build();

    app.renderer.set_disable(EnableOption::DepthTest);

    // Load scene
    let shape_list = ShapeList::from_files(
        arguments.files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: false,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    // - Camera
    let mut camera = FreeCamera::new(
        Point3::new(-0.51, 0.15, 0.0),
        Rad(0.0),
        Rad(0.0),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Rad(0.7175409424302455f32),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    )
    .chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    // - Sun
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(std::f32::consts::PI / 2.0f32),
                height_angle: Rad(std::f32::consts::PI / 2.0f32),
            },
            8192,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .chain_err(|| "Could not render shadowmap")?;

    // Deferred
    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;

    // - Camera
    camera.update_buffer().chain_err(|| "Could not update buffer")?;

    app.renderer.set_viewport(Viewport {
        x: 0,
        y: 0,
        width: app.width,
        height: app.height,
    });

    let mut timer_query = TimerQuery::new();

    let number_of_runs = 10;

    for levels in 5..=11 {
        println!("{}", levels);

        let mut options = VXGIOptions::new();
        options.set_levels(levels);
        options.set_hdr();
        options.voxel_structure = VoxelStructure::SparseVoxelOctree;

        let mut vxgi_module = IsotropicSVOGI::new(options).chain_err(|| "Could not create VXGI module")?;
        vxgi_module
            .voxelize(&mut app.renderer, &[&shape_list], true)
            .chain_err(|| "Could not voxelize geometry")?;
        println!(
            "Voxel Fragment List Count: {:.2}",
            (vxgi_module.voxel_fragment_list().count * 16) as f64 / 1024.0f64 / 1024.0f64
        );
        println!(
            "Nodepool Count: {:.2}",
            (vxgi_module.nodepool().count * 16) as f64 / 1024.0f64 / 1024.0f64
        );
        println!("Brickpool Voxels: {}", vxgi_module.nodepool().count * 27);

        app.swap_buffers();
    }

    Ok(())
}
