mod app_ui {
    use mallumo::conrod::*;
    use mallumo::*;
    use std;

    widget_ids! {
        struct Ids {
            canvas,
            slider_camera_angle_x_title,
            slider_camera_angle_x,
            slider_camera_angle_y_title,
            slider_camera_angle_y,
        }
    }

    pub struct Variables {
        pub camera_angle_x: f32,
        pub camera_angle_y: f32,
        pub update_camera: bool,
    }

    pub struct AppUi {
        ui: Ui,
        ids: self::Ids,
        image_map: image::Map<Texture2D>,
        pub variables: self::Variables,
    }

    impl AppUi {
        pub fn new(width: usize, height: usize) -> AppUi {
            let mut ui = UiBuilder::new([width as f64, height as f64]).build();

            let ids = self::Ids::new(ui.widget_id_generator());

            // Add fonts to ui
            // ...

            // The image map describing widget->image mappings
            let image_map = image::Map::<Texture2D>::new();

            AppUi {
                ui: ui,
                ids: ids,
                image_map: image_map,
                variables: Variables {
                    camera_angle_x: std::f32::consts::PI / 4.0f32,
                    camera_angle_y: 1.0766133,
                    update_camera: false,
                },
            }
        }

        pub fn get_ui<'a>(&'a self) -> &'a Ui {
            &self.ui
        }

        pub fn get_image_map<'a>(&'a self) -> &'a image::Map<Texture2D> {
            &self.image_map
        }

        pub fn process_event(&mut self, event: glutin::Event, window: &glutin::GlWindow) {
            let input = match convert_event(event, &window) {
                None => return,
                Some(input) => input,
            };

            self.ui.handle_event(input);
        }

        pub fn update_ui(&mut self) {
            let ui = &mut self.ui.set_widgets();

            widget::Canvas::new()
                .w(200.0)
                .pad(10.0)
                .top_left_of(ui.window)
                .color(Color::Rgba(0.2, 0.2, 0.2, 1.0))
                .set(self.ids.canvas, ui);

            if let Some(camera_angle_x) = widget::Slider::new(self.variables.camera_angle_x, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .rgb(1.0, 0.3, 0.6)
                .set(self.ids.slider_camera_angle_x, ui)
            {
                if self.variables.camera_angle_x != camera_angle_x {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_x = camera_angle_x;
            }

            if let Some(camera_angle_y) = widget::Slider::new(self.variables.camera_angle_y, 0.0, std::f32::consts::PI)
                .w_h(180.0, 10.0)
                .top_left_of(self.ids.canvas)
                .down(30.0)
                .rgb(0.0, 0.3, 1.0)
                .set(self.ids.slider_camera_angle_y, ui)
            {
                if self.variables.camera_angle_y != camera_angle_y {
                    self.variables.update_camera = true;
                }
                self.variables.camera_angle_y = camera_angle_y;
            }
        }
    }
}

extern crate mallumo;
extern crate time;
extern crate structopt;

#[macro_use]
extern crate error_chain;

mod errors {
    error_chain! {}
}

use errors::*;
use mallumo::cgmath::*;
use mallumo::glutin::*;
use mallumo::*;

use structopt::StructOpt;

quick_main!(run);

fn run() -> Result<()> {
    let arguments = VXGIArguments::from_args();

    if arguments.files.len() == 0 {
        println!("You must provide at least one scene file!");
        return Ok(());
    }

    let mut app = AppBuilder::new()
        .with_title("VXGI example")
        .with_dimensions(1920, 1080)
        .with_multisampling(4)
        .build();
    let mut input = Input::default();

    app.renderer.set_enable(EnableOption::Multisample);
    app.renderer.set_enable(EnableOption::DepthTest);

    let mut options = VXGIOptions::from_arguments(&arguments);
    options.set_levels(9);
    let options_buffer = MutableBuffer::new(&[options]).chain_err(|| "")?;

    let mut gui_renderer = GuiRenderer::new(app.width, app.height, app.gl_window.hidpi_factor() as f64)
        .chain_err(|| "Unable to create GUI renderer")?;
    let mut app_ui = app_ui::AppUi::new(app.width, app.height);

    let mut deferred_collector =
        DefferedCollector::new(app.width, app.height).chain_err(|| "Could not create deferred collector")?;
    let mut scene_renderer = SceneRenderer::new(&mut app.renderer).chain_err(|| "Could not create scene renderer")?;

    // Camera
    // let mut camera = SphereCamera::new(
    //     SpherePosition {
    //         radius: 0.25,
    //         y_angle: Deg(90.0).into(),
    //         height_angle: Deg(75.0).into(),
    //     },
    //     Point3::new(0.0, 0.0, 0.0),
    //     Vector3::new(0.0, 1.0, 0.0),
    //     Deg(45.0).into(),
    //     app.width as usize,
    //     app.height as usize,
    //     0.0001,
    //     100.0,
    // ).chain_err(|| "Could not create camera")?;

    let mut camera = FreeCamera::new(
        Point3::new(0.0, -0.25, 0.0),
        Rad(0.0),
        Rad(0.0),
        Vector3::new(0.0, 1.0, 0.0),
        0.01,
        Deg(45.0).into(),
        app.width as usize,
        app.height as usize,
        0.01,
        10.0,
    )
    .chain_err(|| "Could not create camera")?;
    camera.update_buffer().chain_err(|| "Unable to update buffer")?;

    // Load Scene
    let files = std::env::args().skip(1).map(|s| s.clone()).collect::<Vec<String>>();

    if files.len() == 0 {
        bail!("usage: gltf-display <PATH>");
    }

    let mut shape_list = ShapeList::from_files(
        files.as_slice(),
        Some(Unitization {
            box_min: Point3::new(-1.0, -1.0, -1.0),
            box_max: Point3::new(1.0, 1.0, 1.0),
            unitize_if_fits: true,
        }),
    )
    .chain_err(|| "Could not create shape list")?;

    // Lights
    let sun_module = SunModule::new().expect("Could not initialize Sun module");
    let mut sun = sun_module
        .create_sun(
            Vector3::new(1.0, 1.0, 1.0),
            SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            },
            4096,
        )
        .expect("Could not initialize the Sun");

    sun.render_shadowmap(&mut app.renderer, &[&shape_list])
        .expect("Could not render shadowmaps");

    // VXGI
    let mut svo_module = IsotropicSVOGI::new(options).chain_err(|| "Could not create SVO module")?;
    let mut vxgi_module = IsotropicTextureGI::new(options).chain_err(|| "Could not create VXGI module")?;
    
    svo_module
        .voxelize(&mut app.renderer, &[&shape_list], true)
        .chain_err(|| "Could not voxelize static geometry")?;
    
    vxgi_module
        .voxelize(&mut app.renderer, &[&shape_list], true)
        .chain_err(|| "Could not voxelize static geometry")?;
    
    vxgi_module
        .clear_radiance(&mut app.renderer)
        .chain_err(|| "Could not clear radiance")?;


    let mut enabled_levels = vec![false, false, false, false, false, false, false, false, false, false, false];
    let mut voxels_level = 0;
    let mut rendering_mode = 1;
    let mut brickpool_rendering = VoxelTextureType::Albedo;
    let mut hide_gui = true;

    let mut previous_time = time::precise_time_ns();
    'render_loop: loop {
        let current_time = time::precise_time_ns();
        let move_time = (current_time as f32) / 1_000_000_000.0f32 / 10.0f32;
        //let delta_time = (current_time - previous_time) as f32 / 1000000000.0;
        let delta_time = 0.0333;

        // Process the events
        for app_event in app.poll_events() {
            app_ui.process_event(app_event.clone(), &app.gl_window);

            match app_event.clone() {
                Event::WindowEvent {
                    event: WindowEvent::CursorMoved { position, .. },
                    ..
                } => {
                    if position.0 > 220.0 {
                        input.process_event(&app_event);
                        camera
                            .process_event(&app_event, &input, delta_time)
                            .chain_err(|| "Could not process event in camera")?;
                    }
                }
                _ => {
                    input.process_event(&app_event);
                    camera
                        .process_event(&app_event, &input, delta_time)
                        .chain_err(|| "Could not process event in camera")?;
                }
            };

            match app_event {
                Event::WindowEvent {
                    event: WindowEvent::Resized(w, h),
                    ..
                } => {
                    app.width = w as usize;
                    app.height = h as usize;

                    app.renderer.set_viewport(Viewport {
                        x: 0,
                        y: 0,
                        width: w as usize,
                        height: h as usize,
                    });

                    deferred_collector.resize(app.width, app.height);

                    gui_renderer
                        .resize(w as usize, h as usize)
                        .chain_err(|| "Could not resize GUI")?;
                }
                Event::WindowEvent {
                    event: WindowEvent::KeyboardInput { input, .. },
                    ..
                } => {
                    if input.state == ElementState::Pressed {
                        if let Some(keycode) = input.virtual_keycode {
                            match keycode {
                                glutin::VirtualKeyCode::Key1 => enabled_levels[0] = !enabled_levels[0],
                                glutin::VirtualKeyCode::Key2 => enabled_levels[1] = !enabled_levels[1],
                                glutin::VirtualKeyCode::Key3 => enabled_levels[2] = !enabled_levels[2],
                                glutin::VirtualKeyCode::Key4 => enabled_levels[3] = !enabled_levels[3],
                                glutin::VirtualKeyCode::Key5 => enabled_levels[4] = !enabled_levels[4],
                                glutin::VirtualKeyCode::Key6 => enabled_levels[5] = !enabled_levels[5],
                                glutin::VirtualKeyCode::Key7 => enabled_levels[6] = !enabled_levels[6],
                                glutin::VirtualKeyCode::Key8 => enabled_levels[7] = !enabled_levels[7],
                                glutin::VirtualKeyCode::Key9 => enabled_levels[8] = !enabled_levels[8],
                                glutin::VirtualKeyCode::Numpad1 => voxels_level = 0,
                                glutin::VirtualKeyCode::Numpad2 => voxels_level = 1,
                                glutin::VirtualKeyCode::Numpad3 => voxels_level = 2,
                                glutin::VirtualKeyCode::Numpad4 => voxels_level = 3,
                                glutin::VirtualKeyCode::Numpad5 => voxels_level = 4,
                                glutin::VirtualKeyCode::Numpad6 => voxels_level = 5,
                                glutin::VirtualKeyCode::Numpad7 => voxels_level = 6,
                                glutin::VirtualKeyCode::Numpad8 => voxels_level = 7,
                                glutin::VirtualKeyCode::Numpad9 => voxels_level = 8,
                                glutin::VirtualKeyCode::B => rendering_mode = 1, // Brickpool
                                glutin::VirtualKeyCode::S => rendering_mode = 0, // Scene
                                glutin::VirtualKeyCode::Q => brickpool_rendering = VoxelTextureType::Albedo,
                                glutin::VirtualKeyCode::T => brickpool_rendering = VoxelTextureType::Radiance,
                                glutin::VirtualKeyCode::H => hide_gui = !hide_gui,
                                _ => {}
                            }
                        }
                    }
                }
                Event::WindowEvent {
                    event: WindowEvent::Closed,
                    ..
                } => break 'render_loop,
                _ => (),
            };
        }

        // Render part of the loop
        camera.update_buffer().chain_err(|| "Could not update buffer")?;

        // Uncomment for real-time per frame light
        if app_ui.variables.update_camera == true {
            sun.set_position(SunPosition {
                y_angle: Rad(app_ui.variables.camera_angle_x),
                height_angle: Rad(app_ui.variables.camera_angle_y),
            })
            .chain_err(|| "Could not set Sun's poisition")?;

            sun.render_shadowmap(&mut app.renderer, &[&shape_list])
                .chain_err(|| "Could not render shadowmaps")?;

            svo_module
                .inject_radiance(&mut app.renderer, &sun)
                .chain_err(|| "Could not inject sun light to brickpool")?;

            app_ui.variables.update_camera = false;
        }

        app.renderer.clear_default_framebuffer(ClearBuffers::ColorDepth);

        if rendering_mode == 1 {
            vxgi_module.render_voxels(
                &mut app.renderer,
                &camera,
                voxels_level as usize,
                brickpool_rendering,
            ).chain_err(|| "")?;
        } else {
            deferred_collector
                .render(&mut app.renderer, &[&shape_list], &camera)
                .chain_err(|| "Could not render deferred textures")?;

            scene_renderer.render_fast(&mut app.renderer, &mut shape_list, &camera, None).chain_err(||"")?;
        }

        for i in 0..options.levels() as usize {
            if enabled_levels[i] {
                svo_module
                    .render_nodepool(&mut app.renderer, &camera, i)
                    .chain_err(|| "")?;
            }
        }

        // Update UI
        if hide_gui == false {
            app_ui.update_ui();
            gui_renderer
                .render(&mut app.renderer, &app_ui.get_ui(), &app_ui.get_image_map())
                .chain_err(|| "Could not render GUI")?;
        }

        app.swap_buffers();

        previous_time = current_time;
    }

    Ok(())
}
