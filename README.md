# mallumo

Research real-time rendering engine whose end-goal is to implement and evaluate different algorithms for real-time Global Illumination.

We presented the project at CESCG 2019 and won 2nd Best Presentation Award.

[![](http://img.youtube.com/vi/DabGbA23eCc/0.jpg)](http://www.youtube.com/watch?v=DabGbA23eCc "CESCG 2019: mallumo")

# Requirements

- Rust
- Cargo package manager
- Hardware support for OpenGL 4.5

# Building

All of mallumo libraries are managed by Cargo. Therefore, entire compilation is a just a 'cargo build' command.

## Examples

In order to run examples, use 'cargo run --release --example example_name' command in mallumo folder.
We highly advise running examples in release mode as Rust compiler has very slow debug builds.
Almost all examples require 3d data as input in glTF or OBJ format. You can provide list
with arbitrary number of files and combination of both formats. Good candidates for scene example
with BRDF model and Image Based Lighting are examples from [glTF 2.0 Samples repository](https://github.com/KhronosGroup/glTF-Sample-Models)
such as DamagedHelmet etc. For VXGI and SVO visualization you can try Sponza from our repository.

### Scene

`cargo run --release --example scene -- path_to_file`

Shows scene with Image Based Lighting and Cook Torrance BRDF model.

![Scene with Damaged Helmet from glTF 2.0 Samples](screenshots/scene.jpg)

### Scene dynamic

`cargo run --release --example scene_dynamic`

Shows dynamicity of components. No input is needed for this example.

![Scene with dynamic light and objects](screenshots/scene_dynamic.gif)

### Voxel Based Global Illumination

`cargo run --release --example vxgi -- assets/sponza/sponza.gltf`

Show Voxel Based Global Illumination. Use `--help` to see all the options. Alternatively, you can use `h` key to bring up the UI where you can change any option at run-time.

![Sponza with VXGI](screenshots/vxgi_gui.png)

# Documentation

Can be found at https://docs.rs/mallumo/0.2.0/mallumo/.

# License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

# Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.