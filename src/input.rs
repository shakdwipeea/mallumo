use super::*;

/// Combines Keyboard and Mouse state into one structure.
#[derive(Default, Debug, Copy, Clone, PartialEq)]
pub struct Input {
    pub keyboard: Keyboard,
    pub mouse: Mouse,
}

impl Input {
    /// Passes event to Keyboard and Mouse.
    pub fn process_event(&mut self, event: &glutin::Event) {
        match *event {
            glutin::Event::WindowEvent { ref event, .. } => {
                self.keyboard.process_event(event);
                self.mouse.process_event(event);
            }
            _ => {}
        }
    }
}

/// Represents state of button.
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ButtonState {
    Released,
    Pressed,
}

impl From<glutin::ElementState> for ButtonState {
    fn from(e: glutin::ElementState) -> ButtonState {
        match e {
            glutin::ElementState::Released => ButtonState::Released,
            glutin::ElementState::Pressed => ButtonState::Pressed,
        }
    }
}

impl Default for ButtonState {
    fn default() -> ButtonState {
        ButtonState::Released
    }
}

impl ButtonState {
    pub fn is_released(&self) -> bool {
        match *self {
            ButtonState::Released => true,
            _ => false,
        }
    }

    pub fn is_pressed(&self) -> bool {
        match *self {
            ButtonState::Pressed => true,
            _ => false,
        }
    }
}

/// Describes which keys on keyboard are pressed or released.
#[derive(Default, Debug, Copy, Clone, PartialEq)]
#[allow(non_snake_case)]
pub struct Keyboard {
    pub Key1: ButtonState,
    pub Key2: ButtonState,
    pub Key3: ButtonState,
    pub Key4: ButtonState,
    pub Key5: ButtonState,
    pub Key6: ButtonState,
    pub Key7: ButtonState,
    pub Key8: ButtonState,
    pub Key9: ButtonState,
    pub Key0: ButtonState,
    pub A: ButtonState,
    pub B: ButtonState,
    pub C: ButtonState,
    pub D: ButtonState,
    pub E: ButtonState,
    pub F: ButtonState,
    pub G: ButtonState,
    pub H: ButtonState,
    pub I: ButtonState,
    pub J: ButtonState,
    pub K: ButtonState,
    pub L: ButtonState,
    pub M: ButtonState,
    pub N: ButtonState,
    pub O: ButtonState,
    pub P: ButtonState,
    pub Q: ButtonState,
    pub R: ButtonState,
    pub S: ButtonState,
    pub T: ButtonState,
    pub U: ButtonState,
    pub V: ButtonState,
    pub W: ButtonState,
    pub X: ButtonState,
    pub Y: ButtonState,
    pub Z: ButtonState,
    pub Escape: ButtonState,
    pub F1: ButtonState,
    pub F2: ButtonState,
    pub F3: ButtonState,
    pub F4: ButtonState,
    pub F5: ButtonState,
    pub F6: ButtonState,
    pub F7: ButtonState,
    pub F8: ButtonState,
    pub F9: ButtonState,
    pub F10: ButtonState,
    pub F11: ButtonState,
    pub F12: ButtonState,
    pub F13: ButtonState,
    pub F14: ButtonState,
    pub F15: ButtonState,
    pub Snapshot: ButtonState,
    pub Scroll: ButtonState,
    pub Pause: ButtonState,
    pub Insert: ButtonState,
    pub Home: ButtonState,
    pub Delete: ButtonState,
    pub End: ButtonState,
    pub PageDown: ButtonState,
    pub PageUp: ButtonState,
    pub Left: ButtonState,
    pub Up: ButtonState,
    pub Right: ButtonState,
    pub Down: ButtonState,
    pub Back: ButtonState,
    pub Return: ButtonState,
    pub Space: ButtonState,
    pub Compose: ButtonState,
    pub Numlock: ButtonState,
    pub Numpad0: ButtonState,
    pub Numpad1: ButtonState,
    pub Numpad2: ButtonState,
    pub Numpad3: ButtonState,
    pub Numpad4: ButtonState,
    pub Numpad5: ButtonState,
    pub Numpad6: ButtonState,
    pub Numpad7: ButtonState,
    pub Numpad8: ButtonState,
    pub Numpad9: ButtonState,
    pub AbntC1: ButtonState,
    pub AbntC2: ButtonState,
    pub Add: ButtonState,
    pub Apostrophe: ButtonState,
    pub Apps: ButtonState,
    pub At: ButtonState,
    pub Ax: ButtonState,
    pub Backslash: ButtonState,
    pub Calculator: ButtonState,
    pub Capital: ButtonState,
    pub Colon: ButtonState,
    pub Comma: ButtonState,
    pub Convert: ButtonState,
    pub Decimal: ButtonState,
    pub Divide: ButtonState,
    pub Equals: ButtonState,
    pub Grave: ButtonState,
    pub Kana: ButtonState,
    pub Kanji: ButtonState,
    pub LAlt: ButtonState,
    pub LBracket: ButtonState,
    pub LControl: ButtonState,
    pub LMenu: ButtonState,
    pub LShift: ButtonState,
    pub LWin: ButtonState,
    pub Mail: ButtonState,
    pub MediaSelect: ButtonState,
    pub MediaStop: ButtonState,
    pub Minus: ButtonState,
    pub Multiply: ButtonState,
    pub Mute: ButtonState,
    pub MyComputer: ButtonState,
    pub NavigateForward: ButtonState,
    pub NavigateBackward: ButtonState,
    pub NextTrack: ButtonState,
    pub NoConvert: ButtonState,
    pub NumpadComma: ButtonState,
    pub NumpadEnter: ButtonState,
    pub NumpadEquals: ButtonState,
    pub OEM102: ButtonState,
    pub Period: ButtonState,
    pub PlayPause: ButtonState,
    pub Power: ButtonState,
    pub PrevTrack: ButtonState,
    pub RAlt: ButtonState,
    pub RBracket: ButtonState,
    pub RControl: ButtonState,
    pub RMenu: ButtonState,
    pub RShift: ButtonState,
    pub RWin: ButtonState,
    pub Semicolon: ButtonState,
    pub Slash: ButtonState,
    pub Sleep: ButtonState,
    pub Stop: ButtonState,
    pub Subtract: ButtonState,
    pub Sysrq: ButtonState,
    pub Tab: ButtonState,
    pub Underline: ButtonState,
    pub Unlabeled: ButtonState,
    pub VolumeDown: ButtonState,
    pub VolumeUp: ButtonState,
    pub Wake: ButtonState,
    pub WebBack: ButtonState,
    pub WebFavorites: ButtonState,
    pub WebForward: ButtonState,
    pub WebHome: ButtonState,
    pub WebRefresh: ButtonState,
    pub WebSearch: ButtonState,
    pub WebStop: ButtonState,
    pub Yen: ButtonState,
}

impl Keyboard {
    /// Modifies key state based on passed event.
    pub fn process_event(&mut self, event: &glutin::WindowEvent) {
        match *event {
            glutin::WindowEvent::KeyboardInput { input, .. } => {
                if let Some(keycode) = input.virtual_keycode {
                    match keycode {
                        glutin::VirtualKeyCode::Key1 => self.Key1 = input.state.into(),
                        glutin::VirtualKeyCode::Key2 => self.Key2 = input.state.into(),
                        glutin::VirtualKeyCode::Key3 => self.Key3 = input.state.into(),
                        glutin::VirtualKeyCode::Key4 => self.Key4 = input.state.into(),
                        glutin::VirtualKeyCode::Key5 => self.Key5 = input.state.into(),
                        glutin::VirtualKeyCode::Key6 => self.Key6 = input.state.into(),
                        glutin::VirtualKeyCode::Key7 => self.Key7 = input.state.into(),
                        glutin::VirtualKeyCode::Key8 => self.Key8 = input.state.into(),
                        glutin::VirtualKeyCode::Key9 => self.Key9 = input.state.into(),
                        glutin::VirtualKeyCode::Key0 => self.Key0 = input.state.into(),
                        glutin::VirtualKeyCode::A => self.A = input.state.into(),
                        glutin::VirtualKeyCode::B => self.B = input.state.into(),
                        glutin::VirtualKeyCode::C => self.C = input.state.into(),
                        glutin::VirtualKeyCode::D => self.D = input.state.into(),
                        glutin::VirtualKeyCode::E => self.E = input.state.into(),
                        glutin::VirtualKeyCode::F => self.F = input.state.into(),
                        glutin::VirtualKeyCode::G => self.G = input.state.into(),
                        glutin::VirtualKeyCode::H => self.H = input.state.into(),
                        glutin::VirtualKeyCode::I => self.I = input.state.into(),
                        glutin::VirtualKeyCode::J => self.J = input.state.into(),
                        glutin::VirtualKeyCode::K => self.K = input.state.into(),
                        glutin::VirtualKeyCode::L => self.L = input.state.into(),
                        glutin::VirtualKeyCode::M => self.M = input.state.into(),
                        glutin::VirtualKeyCode::N => self.N = input.state.into(),
                        glutin::VirtualKeyCode::O => self.O = input.state.into(),
                        glutin::VirtualKeyCode::P => self.P = input.state.into(),
                        glutin::VirtualKeyCode::Q => self.Q = input.state.into(),
                        glutin::VirtualKeyCode::R => self.R = input.state.into(),
                        glutin::VirtualKeyCode::S => self.S = input.state.into(),
                        glutin::VirtualKeyCode::T => self.T = input.state.into(),
                        glutin::VirtualKeyCode::U => self.U = input.state.into(),
                        glutin::VirtualKeyCode::V => self.V = input.state.into(),
                        glutin::VirtualKeyCode::W => self.W = input.state.into(),
                        glutin::VirtualKeyCode::X => self.X = input.state.into(),
                        glutin::VirtualKeyCode::Y => self.Y = input.state.into(),
                        glutin::VirtualKeyCode::Z => self.Z = input.state.into(),
                        glutin::VirtualKeyCode::Escape => self.Escape = input.state.into(),
                        glutin::VirtualKeyCode::F1 => self.F1 = input.state.into(),
                        glutin::VirtualKeyCode::F2 => self.F2 = input.state.into(),
                        glutin::VirtualKeyCode::F3 => self.F3 = input.state.into(),
                        glutin::VirtualKeyCode::F4 => self.F4 = input.state.into(),
                        glutin::VirtualKeyCode::F5 => self.F5 = input.state.into(),
                        glutin::VirtualKeyCode::F6 => self.F6 = input.state.into(),
                        glutin::VirtualKeyCode::F7 => self.F7 = input.state.into(),
                        glutin::VirtualKeyCode::F8 => self.F8 = input.state.into(),
                        glutin::VirtualKeyCode::F9 => self.F9 = input.state.into(),
                        glutin::VirtualKeyCode::F10 => self.F10 = input.state.into(),
                        glutin::VirtualKeyCode::F11 => self.F11 = input.state.into(),
                        glutin::VirtualKeyCode::F12 => self.F12 = input.state.into(),
                        glutin::VirtualKeyCode::F13 => self.F13 = input.state.into(),
                        glutin::VirtualKeyCode::F14 => self.F14 = input.state.into(),
                        glutin::VirtualKeyCode::F15 => self.F15 = input.state.into(),
                        glutin::VirtualKeyCode::Snapshot => self.Snapshot = input.state.into(),
                        glutin::VirtualKeyCode::Scroll => self.Scroll = input.state.into(),
                        glutin::VirtualKeyCode::Pause => self.Pause = input.state.into(),
                        glutin::VirtualKeyCode::Insert => self.Insert = input.state.into(),
                        glutin::VirtualKeyCode::Home => self.Home = input.state.into(),
                        glutin::VirtualKeyCode::Delete => self.Delete = input.state.into(),
                        glutin::VirtualKeyCode::End => self.End = input.state.into(),
                        glutin::VirtualKeyCode::PageDown => self.PageDown = input.state.into(),
                        glutin::VirtualKeyCode::PageUp => self.PageUp = input.state.into(),
                        glutin::VirtualKeyCode::Left => self.Left = input.state.into(),
                        glutin::VirtualKeyCode::Up => self.Up = input.state.into(),
                        glutin::VirtualKeyCode::Right => self.Right = input.state.into(),
                        glutin::VirtualKeyCode::Down => self.Down = input.state.into(),
                        glutin::VirtualKeyCode::Back => self.Back = input.state.into(),
                        glutin::VirtualKeyCode::Return => self.Return = input.state.into(),
                        glutin::VirtualKeyCode::Space => self.Space = input.state.into(),
                        glutin::VirtualKeyCode::Compose => self.Compose = input.state.into(),
                        glutin::VirtualKeyCode::Numlock => self.Numlock = input.state.into(),
                        glutin::VirtualKeyCode::Numpad0 => self.Numpad0 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad1 => self.Numpad1 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad2 => self.Numpad2 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad3 => self.Numpad3 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad4 => self.Numpad4 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad5 => self.Numpad5 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad6 => self.Numpad6 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad7 => self.Numpad7 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad8 => self.Numpad8 = input.state.into(),
                        glutin::VirtualKeyCode::Numpad9 => self.Numpad9 = input.state.into(),
                        glutin::VirtualKeyCode::AbntC1 => self.AbntC1 = input.state.into(),
                        glutin::VirtualKeyCode::AbntC2 => self.AbntC2 = input.state.into(),
                        glutin::VirtualKeyCode::Add => self.Add = input.state.into(),
                        glutin::VirtualKeyCode::Apostrophe => self.Apostrophe = input.state.into(),
                        glutin::VirtualKeyCode::Apps => self.Apps = input.state.into(),
                        glutin::VirtualKeyCode::At => self.At = input.state.into(),
                        glutin::VirtualKeyCode::Ax => self.Ax = input.state.into(),
                        glutin::VirtualKeyCode::Backslash => self.Backslash = input.state.into(),
                        glutin::VirtualKeyCode::Calculator => self.Calculator = input.state.into(),
                        glutin::VirtualKeyCode::Capital => self.Capital = input.state.into(),
                        glutin::VirtualKeyCode::Colon => self.Colon = input.state.into(),
                        glutin::VirtualKeyCode::Comma => self.Comma = input.state.into(),
                        glutin::VirtualKeyCode::Convert => self.Convert = input.state.into(),
                        glutin::VirtualKeyCode::Decimal => self.Decimal = input.state.into(),
                        glutin::VirtualKeyCode::Divide => self.Divide = input.state.into(),
                        glutin::VirtualKeyCode::Equals => self.Equals = input.state.into(),
                        glutin::VirtualKeyCode::Grave => self.Grave = input.state.into(),
                        glutin::VirtualKeyCode::Kana => self.Kana = input.state.into(),
                        glutin::VirtualKeyCode::Kanji => self.Kanji = input.state.into(),
                        glutin::VirtualKeyCode::LAlt => self.LAlt = input.state.into(),
                        glutin::VirtualKeyCode::LBracket => self.LBracket = input.state.into(),
                        glutin::VirtualKeyCode::LControl => self.LControl = input.state.into(),
                        glutin::VirtualKeyCode::LMenu => self.LMenu = input.state.into(),
                        glutin::VirtualKeyCode::LShift => self.LShift = input.state.into(),
                        glutin::VirtualKeyCode::LWin => self.LWin = input.state.into(),
                        glutin::VirtualKeyCode::Mail => self.Mail = input.state.into(),
                        glutin::VirtualKeyCode::MediaSelect => self.MediaSelect = input.state.into(),
                        glutin::VirtualKeyCode::MediaStop => self.MediaStop = input.state.into(),
                        glutin::VirtualKeyCode::Minus => self.Minus = input.state.into(),
                        glutin::VirtualKeyCode::Multiply => self.Multiply = input.state.into(),
                        glutin::VirtualKeyCode::Mute => self.Mute = input.state.into(),
                        glutin::VirtualKeyCode::MyComputer => self.MyComputer = input.state.into(),
                        glutin::VirtualKeyCode::NavigateForward => self.NavigateForward = input.state.into(),
                        glutin::VirtualKeyCode::NavigateBackward => self.NavigateBackward = input.state.into(),
                        glutin::VirtualKeyCode::NextTrack => self.NextTrack = input.state.into(),
                        glutin::VirtualKeyCode::NoConvert => self.NoConvert = input.state.into(),
                        glutin::VirtualKeyCode::NumpadComma => self.NumpadComma = input.state.into(),
                        glutin::VirtualKeyCode::NumpadEnter => self.NumpadEnter = input.state.into(),
                        glutin::VirtualKeyCode::NumpadEquals => self.NumpadEquals = input.state.into(),
                        glutin::VirtualKeyCode::OEM102 => self.OEM102 = input.state.into(),
                        glutin::VirtualKeyCode::Period => self.Period = input.state.into(),
                        glutin::VirtualKeyCode::PlayPause => self.PlayPause = input.state.into(),
                        glutin::VirtualKeyCode::Power => self.Power = input.state.into(),
                        glutin::VirtualKeyCode::PrevTrack => self.PrevTrack = input.state.into(),
                        glutin::VirtualKeyCode::RAlt => self.RAlt = input.state.into(),
                        glutin::VirtualKeyCode::RBracket => self.RBracket = input.state.into(),
                        glutin::VirtualKeyCode::RControl => self.RControl = input.state.into(),
                        glutin::VirtualKeyCode::RMenu => self.RMenu = input.state.into(),
                        glutin::VirtualKeyCode::RShift => self.RShift = input.state.into(),
                        glutin::VirtualKeyCode::RWin => self.RWin = input.state.into(),
                        glutin::VirtualKeyCode::Semicolon => self.Semicolon = input.state.into(),
                        glutin::VirtualKeyCode::Slash => self.Slash = input.state.into(),
                        glutin::VirtualKeyCode::Sleep => self.Sleep = input.state.into(),
                        glutin::VirtualKeyCode::Stop => self.Stop = input.state.into(),
                        glutin::VirtualKeyCode::Subtract => self.Subtract = input.state.into(),
                        glutin::VirtualKeyCode::Sysrq => self.Sysrq = input.state.into(),
                        glutin::VirtualKeyCode::Tab => self.Tab = input.state.into(),
                        glutin::VirtualKeyCode::Underline => self.Underline = input.state.into(),
                        glutin::VirtualKeyCode::Unlabeled => self.Unlabeled = input.state.into(),
                        glutin::VirtualKeyCode::VolumeDown => self.VolumeDown = input.state.into(),
                        glutin::VirtualKeyCode::VolumeUp => self.VolumeUp = input.state.into(),
                        glutin::VirtualKeyCode::Wake => self.Wake = input.state.into(),
                        glutin::VirtualKeyCode::WebBack => self.WebBack = input.state.into(),
                        glutin::VirtualKeyCode::WebFavorites => self.WebFavorites = input.state.into(),
                        glutin::VirtualKeyCode::WebForward => self.WebForward = input.state.into(),
                        glutin::VirtualKeyCode::WebHome => self.WebHome = input.state.into(),
                        glutin::VirtualKeyCode::WebRefresh => self.WebRefresh = input.state.into(),
                        glutin::VirtualKeyCode::WebSearch => self.WebSearch = input.state.into(),
                        glutin::VirtualKeyCode::WebStop => self.WebStop = input.state.into(),
                        glutin::VirtualKeyCode::Yen => self.Yen = input.state.into(),
                    }
                }
            }
            _ => (),
        }
    }
}

/// Describes position and previous position of cursor and state of mouse keys.
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mouse {
    pub previous_position: (i32, i32),
    pub position: (i32, i32),
    pub left_button: ButtonState,
    pub right_button: ButtonState,
    pub middle_button: ButtonState,
}

impl Default for Mouse {
    fn default() -> Mouse {
        Mouse {
            previous_position: (0, 0),
            position: (0, 0),
            left_button: ButtonState::Released,
            right_button: ButtonState::Released,
            middle_button: ButtonState::Released,
        }
    }
}

impl Mouse {
    /// Modifies position and key state based on passed event.
    pub fn process_event(&mut self, event: &glutin::WindowEvent) {
        match *event {
            glutin::WindowEvent::CursorMoved { position, .. } => {
                self.previous_position = (self.position.0, self.position.1);
                self.position = (position.0 as i32, position.1 as i32);
            }
            glutin::WindowEvent::MouseInput { state, button, .. } => {
                if button == glutin::MouseButton::Left {
                    self.left_button = match state {
                        glutin::ElementState::Pressed => ButtonState::Pressed,
                        glutin::ElementState::Released => ButtonState::Released,
                    };
                } else if button == glutin::MouseButton::Right {
                    self.right_button = match state {
                        glutin::ElementState::Pressed => ButtonState::Pressed,
                        glutin::ElementState::Released => ButtonState::Released,
                    };
                }
            }
            _ => (),
        }
    }
}
