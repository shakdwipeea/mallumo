use camera::*;
use errors::*;
use ibl::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;
use sun::*;

/// Structure used to render scene supporting different modes for rendering.
///
/// Scene Renderer contains its own pipelines and other structures used to render given scene.
pub struct SceneRenderer {
    brdf_lut: Texture2D,

    globals_buffer: MutableBuffer,
}

impl SceneRenderer {
    /// Creates new Scene Renderer.
    ///
    /// This function needs to have access to Renderer because Scene Renderer creates its own LUT textures/buffers.
    pub fn new(mut renderer: &mut Renderer) -> Result<SceneRenderer> {
        let brdf_lut = integrate_brdf(&mut renderer, 512).chain_err(|| "Could not integrate BRDF")?;

        let globals_buffer =
            MutableBuffer::new(&[0u32, 0u32, 0u32, 0u32]).chain_err(|| "Could not create scene globals buffer")?;

        Ok(SceneRenderer {
            brdf_lut: brdf_lut,

            globals_buffer: globals_buffer,
        })
    }

    /// Creates new Scene Renderer with given BRDF model.
    ///
    /// This function needs to have access to Renderer because Scene Renderer creates its own LUT textures/buffers.
    // pub fn with_custom_brdf(mut renderer: &mut Renderer, brdf: &str) -> Result<SceneRenderer> {
    //     let vertex_shader = Shader::new(
    //         ShaderType::Vertex,
    //         &[
    //             include_str!("../assets/shaders/libs/version.glsl"),
    //             include_str!("../assets/shaders/libs/consts.glsl"),
    //             include_str!("../assets/shaders/libs/vertices.glsl"),
    //             include_str!("../assets/shaders/libs/parameters.glsl"),
    //             include_str!("../assets/shaders/main.vert"),
    //         ],
    //     ).chain_err(|| "Failed to compile vertex shader")?;
    //     let fragment_shader = Shader::new(
    //         ShaderType::Fragment,
    //         &[
    //             include_str!("../assets/shaders/libs/version.glsl"),
    //             include_str!("../assets/shaders/libs/consts.glsl"),
    //             include_str!("../assets/shaders/libs/vertices.glsl"),
    //             include_str!("../assets/shaders/libs/parameters.glsl"),
    //             brdf,
    //             include_str!("../assets/shaders/main.frag"),
    //         ],
    //     ).chain_err(|| "Failed to compile fragment shader")?;

    //     let pipeline = PipelineBuilder::new()
    //         .vertex_shader(&vertex_shader)
    //         .fragment_shader(&fragment_shader)
    //         .build()
    //         .chain_err(|| "Unable to build pipeline")?;

    //     let brdf_lut = integrate_brdf(&mut renderer, 512).chain_err(|| "Could not integrate BRDF")?;

    //     let globals_buffer =
    //         MutableBuffer::new(&[0u32, 0u32, 0u32, 0u32]).chain_err(|| "Could not create scene globals buffer")?;

    //     Ok(SceneRenderer {
    //         brdf_lut: brdf_lut,

    //         globals_buffer: globals_buffer,
    //     })
    // }

    /// Renders scene with IBL mode.
    pub fn render<T: Camera>(
        &mut self,
        renderer: &mut Renderer,
        shape_lists: &[&ShapeList],
        camera: &T,
        ibl: Option<(&TextureCubemap, &TextureCubemap)>,
    ) -> Result<()> {
        let pipeline = &*fetch_program("main");

        for shape_list in shape_lists {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                if let Some((diffuse, specular)) = ibl {
                    let mut draw_command = DrawCommand::arrays(pipeline, 0, shape.indices.1);
                    draw_command = draw_command
                        .uniform(camera.get_buffer(), 0)
                        .storage_range_read::<u32>(&shape_list.indices_buffer, 1, indices.0, indices.1)
                        .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 2, vertices.0, vertices.1)
                        .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 3, i, 1)
                        .texture_2d(shape_list.texture(shape.albedo), 4)
                        .texture_2d(shape_list.texture(shape.metallic_roughness), 5)
                        .texture_2d(shape_list.texture(shape.occlusion), 6)
                        .texture_2d(shape_list.texture(shape.normal), 7)
                        .texture_2d(shape_list.texture(shape.emissive), 8)
                        .texture_2d(Some(&self.brdf_lut), 9)
                        .texture_cubemap(diffuse, 10)
                        .texture_cubemap(specular, 11);
                    renderer.draw(&draw_command).chain_err(|| "Could not render scene")?;
                } else {
                    let mut draw_command = DrawCommand::arrays(pipeline, 0, shape.indices.1);
                    draw_command = draw_command
                        .uniform(camera.get_buffer(), 0)
                        .storage_range_read::<u32>(&shape_list.indices_buffer, 1, indices.0, indices.1)
                        .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 2, vertices.0, vertices.1)
                        .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 3, i, 1)
                        .texture_2d(shape_list.texture(shape.albedo), 4)
                        .texture_2d(shape_list.texture(shape.metallic_roughness), 5)
                        .texture_2d(shape_list.texture(shape.occlusion), 6)
                        .texture_2d(shape_list.texture(shape.normal), 7)
                        .texture_2d(shape_list.texture(shape.emissive), 8);
                    renderer.draw(&draw_command).chain_err(|| "Could not render scene")?;
                }
            }
        }

        Ok(())
    }

    pub fn render_fast<T: Camera>(
        &mut self,
        renderer: &mut Renderer,
        shape_list: &mut ShapeList,
        camera: &T,
        sun: Option<&Sun>,
    ) -> Result<()> {
        let pipeline = &*fetch_program("main_fastpath");

        if shape_list.indirect_buffer.is_none() {
            shape_list.generate_indirect_buffer();
        }

        let mut draw_command = DrawCommand::multi_draw_elements_indirect(
            pipeline,
            &shape_list.indices_buffer,
            shape_list.indirect_buffer.as_ref().unwrap(),
            shape_list.shapes.len(),
            0,
        );

        // No point lights
        self.globals_buffer
            .set_sub_data(&[0u32], 4)
            .chain_err(|| "Could not update scene globals buffer")?;

        // Sun
        if let Some(sun) = sun {
            self.globals_buffer
                .set_sub_data(&[1u32], 0)
                .chain_err(|| "Could not update scene globals buffer")?;

            draw_command = draw_command
                .uniform(sun.get_buffer(), 2)
                .texture_2d(sun.get_shadowmap(), 6);
        }

        // Parameters
        draw_command = draw_command
            // Uniforms
            .uniform(&self.globals_buffer, 0)
            .uniform(camera.get_buffer(), 1)
            // Buffers
            .storage_read(&shape_list.vertices_buffer, 1)
            .storage_read(&shape_list.primitive_parameters_buffer, 2);

        // Render
        renderer.draw(&draw_command).chain_err(|| "Could not render scene")?;

        Ok(())
    }
}
