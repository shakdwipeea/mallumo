use shape_list::errors::*;
use shape_list::mikktspace::*;

use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::mem::transmute;
use std::os::raw::*;
use std::slice::{from_raw_parts, from_raw_parts_mut};

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub pad1: u32,
    pub normal: [f32; 3],
    pub pad2: u32,
    pub texture_coordinate: [f32; 2],
    pub pad3: [u32; 2],
    pub tangent: [f32; 4],
}

impl PartialEq for Vertex {
    fn eq(&self, other: &Vertex) -> bool {
        unsafe {
            transmute::<[f32; 3], [u32; 3]>(self.position) == transmute::<[f32; 3], [u32; 3]>(other.position)
                && transmute::<[f32; 3], [u32; 3]>(self.normal) == transmute::<[f32; 3], [u32; 3]>(other.normal)
                && transmute::<[f32; 2], [u32; 2]>(self.texture_coordinate)
                    == transmute::<[f32; 2], [u32; 2]>(other.texture_coordinate)
                && transmute::<[f32; 4], [u32; 4]>(self.tangent) == transmute::<[f32; 4], [u32; 4]>(other.tangent)
        }
    }
}

impl Eq for Vertex {}

impl Hash for Vertex {
    fn hash<H: Hasher>(&self, state: &mut H) {
        unsafe {
            transmute::<[f32; 3], [u32; 3]>(self.position).hash(state);
            transmute::<[f32; 3], [u32; 3]>(self.normal).hash(state);
            transmute::<[f32; 2], [u32; 2]>(self.texture_coordinate).hash(state);
            transmute::<[f32; 4], [u32; 4]>(self.tangent).hash(state);
        }
    }
}

impl Vertex {
    pub fn new(position: [f32; 3], normal: [f32; 3], texture_coordinate: [f32; 2], tangent: [f32; 4]) -> Vertex {
        Vertex {
            position: position,
            normal: normal,
            texture_coordinate: texture_coordinate,
            tangent: tangent,
            pad1: 0,
            pad2: 0,
            pad3: [0, 0],
        }
    }

    pub fn soa_to_aos(
        positions: &[[f32; 3]],
        normals: &[[f32; 3]],
        texture_coordinates: Option<&[[f32; 2]]>,
        tangents: Option<&[[f32; 4]]>,
    ) -> Result<Vec<Vertex>> {
        let mut result = Vec::new();

        if positions.len() != normals.len() {
            bail!("Positions and normals must have the same length");
        }

        let zero2 = vec![[0.0; 2]; positions.len()];
        let zero4 = vec![[0.0; 4]; positions.len()];

        let some_tangents = match tangents {
            Some(t) => t,
            None => &zero4,
        };

        let some_texture_coordinates = match texture_coordinates {
            Some(tc) => tc,
            None => &zero2,
        };

        for i in 0..positions.len() {
            result.push(Vertex::new(
                positions[i],
                normals[i],
                some_texture_coordinates[i],
                some_tangents[i],
            ));
        }

        Ok(result)
    }
}

pub fn index_vertices(vertices: &[Vertex]) -> (Vec<u32>, Vec<Vertex>) {
    let mut indices: HashMap<Vertex, u32> = HashMap::new();

    let mut result: (Vec<u32>, Vec<Vertex>) = (Vec::new(), Vec::new());
    for vertex in vertices {
        let mut idx: u32 = 0;
        let mut found: bool = false;

        if let Some(index) = indices.get(vertex) {
            idx = *index;
            found = true;
        }

        if !found {
            idx = indices.len() as u32;
            result.1.push(*vertex);
            indices.insert(*vertex, idx);
        }

        result.0.push(idx);
    }

    result
}

pub fn deindex_vertices(indices: &[u32], vertices: &[Vertex]) -> Vec<Vertex> {
    let mut result: Vec<Vertex> = Vec::new();
    for index in indices.iter() {
        let idx = *index as usize;
        result.push(vertices[idx]);
    }
    result
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_get_num_faces(context: *const SMikkTSpaceContext) -> c_int {
    let vertices: &mut Vec<Vertex>;
    unsafe {
        vertices = &mut *((*context).data as *mut Vec<Vertex>);
    }

    (vertices.len() / 3) as i32
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_get_num_vertices_of_face(context: *const SMikkTSpaceContext, face: c_int) -> c_int {
    3
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_get_position(
    context: *const SMikkTSpaceContext,
    position_out: *mut c_float,
    face_index: c_int,
    vertex_index: c_int,
) -> () {
    let vertices: &mut Vec<Vertex>;
    unsafe {
        vertices = &mut *((*context).data as *mut Vec<Vertex>);
    }

    let vertex = vertices[(face_index * 3 + vertex_index) as usize];
    unsafe {
        let position = from_raw_parts_mut::<c_float>(position_out, 3);
        position[0] = vertex.position[0];
        position[1] = vertex.position[1];
        position[2] = vertex.position[2];
    }
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_get_normal(
    context: *const SMikkTSpaceContext,
    normal_out: *mut c_float,
    face_index: c_int,
    vertex_index: c_int,
) -> () {
    let vertices: &mut Vec<Vertex>;
    unsafe {
        vertices = &mut *((*context).data as *mut Vec<Vertex>);
    }

    let vertex = vertices[(face_index * 3 + vertex_index) as usize];
    unsafe {
        let normal = from_raw_parts_mut::<c_float>(normal_out, 3);
        normal[0] = vertex.normal[0];
        normal[1] = vertex.normal[1];
        normal[2] = vertex.normal[2];
    }
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_get_texcoord(
    context: *const SMikkTSpaceContext,
    texture_coordinate_out: *mut c_float,
    face_index: c_int,
    vertex_index: c_int,
) -> () {
    let vertices: &mut Vec<Vertex>;
    unsafe {
        vertices = &mut *((*context).data as *mut Vec<Vertex>);
    }

    let vertex = vertices[(face_index * 3 + vertex_index) as usize];
    unsafe {
        let texture_coordinate = from_raw_parts_mut::<c_float>(texture_coordinate_out, 3);
        texture_coordinate[0] = vertex.texture_coordinate[0];
        texture_coordinate[1] = vertex.texture_coordinate[1];
    }
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_set_tspace_basic(
    context: *const SMikkTSpaceContext,
    tangent_in: *const c_float,
    sign: c_float,
    face_index: c_int,
    vertex_index: c_int,
) -> () {
    let vertices: &mut Vec<Vertex>;
    unsafe {
        vertices = &mut *((*context).data as *mut Vec<Vertex>);
    }

    let tangent;
    unsafe {
        tangent = from_raw_parts::<c_float>(tangent_in, 3);
    }

    let vertex: &mut Vertex = &mut vertices[(face_index * 3 + vertex_index) as usize];
    vertex.tangent = [tangent[0] * sign, tangent[1], tangent[2], sign];
}

#[allow(unused_variables)]
pub extern "C" fn mikkt_set_tspace(
    context: *const SMikkTSpaceContext,
    tangent_in: *const c_float,
    bitangent_in: *const c_float,
    mag_s: c_float,
    mag_t: c_float,
    orientation_preserving: c_int,
    face_index: c_int,
    vertex_index: c_int,
) -> () {
}
