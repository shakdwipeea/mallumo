use super::errors::*;
use super::*;

use std::cell::Cell;

use cgmath;

/// Simple dynamic camera which is controlled with mouse and arrow keys.
#[derive(Debug)]
pub struct FreeCamera {
    pub position: cgmath::Point3<f32>,
    pub yaw: cgmath::Rad<f32>,
    pub pitch: cgmath::Rad<f32>,
    pub up: cgmath::Vector3<f32>,

    pub speed: f32,

    pub fov: cgmath::Rad<f32>,
    pub width: usize,
    pub height: usize,
    pub near: f32,
    pub far: f32,
    pub camera_buffer: MutableBuffer,

    change_p_matrix: Cell<bool>,
    change_v_matrix: Cell<bool>,
    change_position: Cell<bool>,
}

impl Camera for FreeCamera {
    fn update_buffer(&mut self) -> Result<()> {
        let change_p_matrix = self.change_p_matrix.get();
        let change_v_matrix = self.change_v_matrix.get();
        let change_pv_matrix = self.change_p_matrix.get() || self.change_v_matrix.get();
        let change_position = self.change_position.get();

        if change_p_matrix {
            let p_matrix = self.get_projection();
            self.camera_buffer
                .set_sub_data(p_matrix.as_ref() as &[f32; 16], 0)
                .chain_err(|| "Unable to update projection matrix")?;
            self.change_p_matrix.set(false);
        }

        if change_v_matrix {
            let v_matrix = self.get_view();
            self.camera_buffer
                .set_sub_data(v_matrix.as_ref() as &[f32; 16], 64)
                .chain_err(|| "Unable to update view matrix")?;
            self.change_v_matrix.set(false);
        }

        if change_pv_matrix {
            let pv_matrix = self.get_projection_view();
            self.camera_buffer
                .set_sub_data(pv_matrix.as_ref() as &[f32; 16], 128)
                .chain_err(|| "Unable to update projection view matrix")?;
            self.change_p_matrix.set(false);
            self.change_v_matrix.set(false);
        }

        if change_position {
            let position = self.get_position();
            self.camera_buffer
                .set_sub_data(position.as_ref() as &[f32; 3], 192)
                .chain_err(|| "Unable to update camera position")?;
            self.change_position.set(false);
        }

        Ok(())
    }

    fn get_buffer(&self) -> &Buffer {
        &self.camera_buffer
    }

    fn get_projection(&self) -> cgmath::Matrix4<f32> {
        let f: f32 = 1.0f32 / (self.fov.0 as f32 / 2.0f32).tan();
        cgmath::Matrix4::new(
            f / self.get_ratio(),
            0.0,
            0.0,
            0.0,
            0.0,
            f,
            0.0,
            0.0,
            0.0,
            0.0,
            0.0,
            -1.0,
            0.0,
            0.0,
            self.near,
            0.0,
        )
    }

    fn get_view(&self) -> cgmath::Matrix4<f32> {
        cgmath::Matrix4::look_at(
            self.position,
            self.position + yaw_pitch_to_front(self.yaw, self.pitch),
            self.up,
        )
    }

    fn get_projection_view(&self) -> cgmath::Matrix4<f32> {
        self.get_projection() * self.get_view()
    }

    fn get_position(&self) -> cgmath::Point3<f32> {
        self.position
    }

    fn get_ratio(&self) -> f32 {
        self.width as f32 / self.height as f32
    }
}

fn yaw_pitch_to_front(yaw: cgmath::Rad<f32>, pitch: cgmath::Rad<f32>) -> cgmath::Vector3<f32> {
    use cgmath::prelude::*;
    use cgmath::Rad;

    let x = Rad::cos(yaw) * Rad::cos(pitch);
    let y = Rad::sin(pitch);
    let z = Rad::sin(yaw) * Rad::cos(pitch);

    cgmath::Vector3::new(x, y, z).normalize()
}

impl FreeCamera {
    // TODO: optimize setting data(not all camera transformation change all data)
    /// Creates new Free Camera.
    pub fn new(
        position: cgmath::Point3<f32>,
        yaw: cgmath::Rad<f32>,
        pitch: cgmath::Rad<f32>,
        up: cgmath::Vector3<f32>,
        speed: f32,
        fov: cgmath::Rad<f32>,
        width: usize,
        height: usize,
        near: f32,
        far: f32,
    ) -> Result<FreeCamera> {
        use std::cmp::max;

        let clamped_width = max(1, width);
        let clamped_height = max(1, height);

        let p_matrix = cgmath::perspective(fov, clamped_width as f32 / clamped_height as f32, near, far);
        let v_matrix = cgmath::Matrix4::look_at(position, position + yaw_pitch_to_front(yaw, pitch), up);

        let mut camera_buffer =
            MutableBuffer::new_empty(std::mem::size_of::<f32>() * 52).chain_err(|| "Unable to create camera buffer")?;

        camera_buffer
            .set_sub_data(p_matrix.as_ref() as &[f32; 16], 0)
            .chain_err(|| "Unable to update projection matrix")?;
        camera_buffer
            .set_sub_data(v_matrix.as_ref() as &[f32; 16], 64)
            .chain_err(|| "Unable to update view matrix")?;
        camera_buffer
            .set_sub_data((p_matrix * v_matrix).as_ref() as &[f32; 16], 128)
            .chain_err(|| "Unable to update projection view matrix")?;
        camera_buffer
            .set_sub_data(position.as_ref() as &[f32; 3], 192)
            .chain_err(|| "Unable to update camera position")?;

        Ok(FreeCamera {
            position: position,
            yaw: yaw,
            pitch: pitch,
            up: up,

            speed: speed,

            fov: fov,
            width: width,
            height: height,
            near: near,
            far: far,
            camera_buffer: camera_buffer,

            change_p_matrix: Cell::new(false),
            change_v_matrix: Cell::new(false),
            change_position: Cell::new(false),
        })
    }

    /// Moves to the front taking into account passed time.
    ///
    /// The delta in position is equal to front vector(calculated from yaw and pitch) * speed * delta_time
    pub fn move_front(&mut self, front: bool, delta_time: f32) -> &mut Self {
        self.change_v_matrix.set(true);
        self.change_position.set(true);

        let front_vector = yaw_pitch_to_front(self.yaw, self.pitch);
        if front {
            self.position += front_vector * self.speed * delta_time;
        } else {
            self.position += -front_vector * self.speed * delta_time;
        }
        self
    }

    /// Moves to the left taking into account passed time.
    ///
    /// The delta in position is equal to side vector(calculated from yaw, pitch and up vector) * speed * delta_time
    pub fn move_left(&mut self, right: bool, delta_time: f32) -> &mut Self {
        use cgmath::InnerSpace;

        self.change_v_matrix.set(true);
        self.change_position.set(true);

        let front_vector = yaw_pitch_to_front(self.yaw, self.pitch);
        if right {
            self.position += -front_vector.cross(self.up).normalize() * self.speed * delta_time;
        } else {
            self.position += front_vector.cross(self.up).normalize() * self.speed * delta_time;
        }
        self
    }

    /// Rotates in respect to Y axis.
    pub fn rot_yaw(&mut self, yaw: cgmath::Rad<f32>) -> &mut Self {
        use cgmath::Angle;
        self.change_v_matrix.set(true);
        self.change_position.set(true);
        self.yaw = (self.yaw + yaw).normalize();
        self
    }

    /// Changes pitch, in a way representing movement in Y direction.
    pub fn rot_pitch(&mut self, pitch: cgmath::Rad<f32>, saturate: bool) -> &mut Self {
        use cgmath::Deg;
        self.change_v_matrix.set(true);
        self.change_position.set(true);

        if saturate {
            if self.pitch + pitch < Deg(-89.9999).into() {
                self.pitch = Deg(-89.9999).into();
            } else if self.pitch + pitch > Deg(89.99999).into() {
                self.pitch = Deg(89.99999).into();
            } else {
                self.pitch += pitch;
            }
        } else {
            self.pitch += pitch;
        }

        self
    }

    /// Processes window events and modifies Mouse and ArrowsController state.
    ///
    /// process_kb_state() should be called after all window events are processed.
    pub fn process_event(&mut self, event: &glutin::Event, input: &Input, delta_time: f32) -> Result<()> {
        let window_event: glutin::WindowEvent;
        match *event {
            glutin::Event::WindowEvent { ref event, .. } => {
                window_event = event.clone();
            }
            _ => {
                return Ok(());
            }
        }

        match window_event {
            glutin::WindowEvent::Resized(width, height) => {
                if width != 0 && height != 0 {
                    self.set_viewport(width as usize, height as usize);
                }
            }
            glutin::WindowEvent::MouseWheel { delta, .. } => match delta {
                glutin::MouseScrollDelta::LineDelta(_, rows) => {
                    let new_speed = self.speed + (rows as f32 / 10.0);
                    if new_speed <= 0.0 {
                        self.set_speed(0.0);
                    } else {
                        self.set_speed(new_speed);
                    }
                }
                glutin::MouseScrollDelta::PixelDelta(horizontal, vertical) => {
                    println!("horizontal: {:?} vertical: {:?}", horizontal, vertical);
                }
            },
            glutin::WindowEvent::CursorMoved { .. } => {
                if input.mouse.left_button == ButtonState::Pressed {
                    let (dx, dy) = (
                        input.mouse.position.0 - input.mouse.previous_position.0,
                        input.mouse.position.1 - input.mouse.previous_position.1,
                    );
                    let (dx, dy) = (dx as f32 / self.width as f32, dy as f32 / self.height as f32);
                    self.rot_yaw(cgmath::Rad(dx * 4.0));
                    self.rot_pitch(cgmath::Rad(-dy * 4.0), true);
                }
            }
            _ => {}
        };

        if input.keyboard.Up == ButtonState::Pressed {
            self.move_front(true, delta_time);
        }
        if input.keyboard.Down == ButtonState::Pressed {
            self.move_front(false, delta_time);
        }
        if input.keyboard.Right == ButtonState::Pressed {
            self.move_left(false, delta_time);
        }
        if input.keyboard.Left == ButtonState::Pressed {
            self.move_left(true, delta_time);
        }

        Ok(())
    }

    pub fn set_position(&mut self, position: cgmath::Point3<f32>) -> &mut Self {
        self.change_p_matrix.set(true);
        self.change_position.set(true);
        self.position = position;
        self
    }

    pub fn get_yaw(&self) -> cgmath::Rad<f32> {
        self.yaw
    }

    pub fn set_yaw(&mut self, yaw: cgmath::Rad<f32>) -> &mut Self {
        self.change_v_matrix.set(true);
        self.yaw = yaw;
        self
    }

    pub fn get_pitch(&self) -> cgmath::Rad<f32> {
        self.pitch
    }

    pub fn set_pitch(&mut self, pitch: cgmath::Rad<f32>) -> &mut Self {
        self.change_v_matrix.set(true);
        self.pitch = pitch;
        self
    }

    pub fn get_up(&self) -> cgmath::Vector3<f32> {
        self.up
    }

    pub fn set_up(&mut self, up: cgmath::Vector3<f32>) -> &mut Self {
        self.change_v_matrix.set(true);
        self.up = up;
        self
    }

    pub fn get_speed(&self) -> f32 {
        self.speed
    }

    pub fn set_speed(&mut self, speed: f32) {
        self.speed = speed;
    }

    pub fn get_fov(&self) -> cgmath::Rad<f32> {
        self.fov
    }

    pub fn set_fov(&mut self, fov: cgmath::Rad<f32>) -> &mut Self {
        self.change_p_matrix.set(true);
        self.fov = fov;
        self
    }

    pub fn get_viewport(&self) -> (usize, usize) {
        (self.width, self.height)
    }

    pub fn set_viewport(&mut self, width: usize, height: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.width = width;
        self.height = height;
        self
    }

    pub fn get_width(&self) -> usize {
        self.width
    }

    pub fn set_width(&mut self, width: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.width = width;
        self
    }

    pub fn get_height(&self) -> usize {
        self.height
    }

    pub fn set_height(&mut self, height: usize) -> &mut Self {
        self.change_p_matrix.set(true);
        self.height = height;
        self
    }

    pub fn get_near(&self) -> f32 {
        self.near
    }

    pub fn set_near(&mut self, near: f32) -> &mut Self {
        self.change_p_matrix.set(true);
        self.near = near;
        self
    }

    pub fn get_far(&self) -> f32 {
        self.far
    }

    pub fn set_far(&mut self, far: f32) -> &mut Self {
        self.change_p_matrix.set(true);
        self.far = far;
        self
    }
}
