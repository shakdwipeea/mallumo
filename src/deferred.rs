use camera::*;
use errors::*;
use mallumo_gls::*;
use shader_loader::*;
use shape_list::*;

/// Holds textures with geometric data.
pub struct DefferedCollector {
    pub depth: Texture2D,
    pub position: Texture2D,
    pub albedo: Texture2D,
    pub orm: Texture2D,
    pub normal: Texture2D,
    pub emission: Texture2D,

    pub framebuffer: GeneralFramebuffer,
}

impl DefferedCollector {
    /// Creates new Deffered Collector.
    pub fn new(width: usize, height: usize) -> Result<DefferedCollector> {
        let mut depth = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::DepthComponent32F,
            TextureFormat::DepthComponent,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create depth texture")?;
        let mut position = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create position texture")?;
        let mut albedo = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGBA8,
            TextureFormat::RGBA,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create albedo texture")?;
        let mut orm = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB8,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create orm texture")?;
        let mut normal = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create normal texture")?;
        let mut emission = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create emission texture")?;

        let mut framebuffer = GeneralFramebuffer::new();
        framebuffer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: width,
            height: height,
        });
        framebuffer.set_clear_color(ClearColor::default());
        framebuffer.set_enable(EnableOption::DepthTest);
        framebuffer.set_enable(EnableOption::CullFace);

        {
            let dtt = DrawTextureTarget {
                depth_stencil: DepthStencilOption::Separate {
                    depth: DrawTextureAttachOption::AttachTexture(&mut depth),
                    stencil: DrawTextureAttachOption::None,
                },
                color0: DrawTextureAttachOption::AttachTexture(&mut position),
                color1: DrawTextureAttachOption::AttachTexture(&mut albedo),
                color2: DrawTextureAttachOption::AttachTexture(&mut orm),
                color3: DrawTextureAttachOption::AttachTexture(&mut normal),
                color4: DrawTextureAttachOption::AttachTexture(&mut emission),
                ..Default::default()
            };
            framebuffer
                .attach_textures(&dtt)
                .chain_err(|| "Could not attach textures")?;
        }

        Ok(DefferedCollector {
            depth: depth,
            position: position,
            albedo: albedo,
            orm: orm,
            normal: normal,
            emission: emission,

            framebuffer: framebuffer,
        })
    }

    /// Renders scene parameters into Textures from the viewpoint of Camera.
    pub fn render(&mut self, renderer: &mut Renderer, shape_lists: &[&ShapeList], camera: &Camera) -> Result<()> {
        let pipeline = &*fetch_program("deferred/deferred_collect");
        let attachments = DrawTextureTarget::keep();

        renderer.clear_framebuffer(&self.framebuffer, ClearBuffers::ColorDepth);

        for shape_list in shape_lists {
            for (i, shape) in shape_list.shapes.iter().enumerate() {
                let indices = shape.indices;
                let vertices = shape.vertices;

                let draw_command = DrawCommand::arrays(pipeline, 0, shape.indices.1)
                    .framebuffer(&self.framebuffer)
                    .attachments(&attachments)
                    .uniform(camera.get_buffer(), 0)
                    .storage_range_read::<u32>(&shape_list.indices_buffer, 0, indices.0, indices.1)
                    .storage_range_read::<Vertex>(&shape_list.vertices_buffer, 1, vertices.0, vertices.1)
                    .storage_range_read::<PrimitiveParameters>(&shape_list.primitive_parameters_buffer, 2, i, 1)
                    .texture_2d(shape_list.texture(shape.albedo), 0)
                    .texture_2d(shape_list.texture(shape.metallic_roughness), 1)
                    .texture_2d(shape_list.texture(shape.occlusion), 2)
                    .texture_2d(shape_list.texture(shape.normal), 3)
                    .texture_2d(shape_list.texture(shape.emissive), 4);

                renderer.draw(&draw_command).chain_err(|| "Could not render g buffer")?;
            }
        }

        Ok(())
    }

    /// Renders scene parameters into Textures from the viewpoint of Camera.
    pub fn render_fast(
        &mut self,
        renderer: &mut Renderer,
        shape_lists: &mut [&mut ShapeList],
        camera: &Camera,
    ) -> Result<()> {
        let pipeline = &*fetch_program("deferred/deferred_collect_fast");
        let attachments = DrawTextureTarget::keep();

        renderer.clear_framebuffer(&self.framebuffer, ClearBuffers::ColorDepth);

        for shape_list in shape_lists {
            if shape_list.indirect_buffer.is_none() {
                shape_list.generate_indirect_buffer();
            }

            let mut draw_command = DrawCommand::multi_draw_elements_indirect(
                pipeline,
                &shape_list.indices_buffer,
                shape_list.indirect_buffer.as_ref().unwrap(),
                shape_list.shapes.len(),
                0,
            );

            let draw_command = draw_command
                .framebuffer(&self.framebuffer)
                .attachments(&attachments)
                .uniform(camera.get_buffer(), 0)
                .storage_read(&shape_list.vertices_buffer, 1)
                .storage_read(&shape_list.primitive_parameters_buffer, 2);

            renderer.draw(&draw_command).chain_err(|| "Could not render g buffer")?;
        }

        Ok(())
    }

    /// Resizes geometric textures.
    ///
    /// Usually this is called after resizing window.
    pub fn resize(&mut self, width: usize, height: usize) -> Result<()> {
        self.framebuffer.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: width,
            height: height,
        });

        let mut depth = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::DepthComponent32F,
            TextureFormat::DepthComponent,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create depth texture")?;
        let mut position = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create position texture")?;
        let mut albedo = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGBA8,
            TextureFormat::RGBA,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create albedo texture")?;
        let mut orm = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB8,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create orm texture")?;
        let mut normal = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create normal texture")?;
        let mut emission = Texture2D::new_empty(
            Texture2DSize(width, height),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::UnsignedByte,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                ..Default::default()
            },
            1,
        )
        .chain_err(|| "Could not create emission texture")?;

        {
            let dtt = DrawTextureTarget {
                depth_stencil: DepthStencilOption::Separate {
                    depth: DrawTextureAttachOption::AttachTexture(&mut depth),
                    stencil: DrawTextureAttachOption::None,
                },
                color0: DrawTextureAttachOption::AttachTexture(&mut position),
                color1: DrawTextureAttachOption::AttachTexture(&mut albedo),
                color2: DrawTextureAttachOption::AttachTexture(&mut orm),
                color3: DrawTextureAttachOption::AttachTexture(&mut normal),
                color4: DrawTextureAttachOption::AttachTexture(&mut emission),
                ..Default::default()
            };
            self.framebuffer
                .attach_textures(&dtt)
                .chain_err(|| "Could not attach textures")?;
        }

        self.depth = depth;
        self.position = position;
        self.albedo = albedo;
        self.orm = orm;
        self.normal = normal;
        self.emission = emission;

        Ok(())
    }
}
