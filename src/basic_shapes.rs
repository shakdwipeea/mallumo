use mallumo_gls::*;
use shape_list::*;

use cgmath::{InnerSpace, Matrix4, One, Point3, Vector3, Vector4};

/// Raw positions of cube vertices.
pub const CUBE_VERTICES: [f32; 32] = [
    -1.0, -1.0, 1.0, 0.0, 1.0, -1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 0.0, -1.0, 1.0, 1.0, 0.0, -1.0, -1.0, -1.0, 0.0, 1.0,
    -1.0, -1.0, 0.0, 1.0, 1.0, -1.0, 0.0, -1.0, 1.0, -1.0, 0.0,
];

/// Cube indices describing how are CUBE_VERTICES ordered. Use these if you are viewing the cube from the outside or you don't use culling.
pub const CUBE_INDICES: [u32; 36] = [
    0, 1, 2, 2, 3, 0, 1, 5, 6, 6, 2, 1, 7, 6, 5, 5, 4, 7, 4, 0, 3, 3, 7, 4, 4, 5, 1, 1, 0, 4, 3, 2, 6, 6, 7, 3,
];

/// Cube indices describing how are CUBE_VERTICES ordererd. Use these if you are viewing the cube from the inside, e.g. skybox.
pub const INVERTED_CUBE_INDICES: [u32; 36] = [
    0, 2, 1, 2, 0, 3, 1, 6, 5, 6, 1, 2, 7, 5, 6, 5, 7, 4, 4, 3, 0, 3, 4, 7, 4, 1, 5, 1, 4, 0, 3, 6, 2, 6, 3, 7,
];

/// Generate cube. Pass the result into ShapeList constructors, e.g. from_shape().
pub fn generate_cube(
    center: Point3<f32>,
    side_length: f32,
    albedo: Vector4<f32>,
    metalness: f32,
    roughness: f32,
    casts_shadows: bool,
    receives_shadows: bool,
) -> (Vec<u32>, Vec<Vertex>, PrimitiveParameters) {
    fn quad(center: Point3<f32>, up: Vector3<f32>, right: Vector3<f32>) -> [Vertex; 4] {
        let down_left = center + (-up) + (-right);
        let up_left = center + up + (-right);
        let up_right = center + up + right;
        let down_right = center + (-up) + right;

        let normal = right.normalize().cross(up.normalize()).normalize();

        [
            Vertex::new(
                [down_left[0], down_left[1], down_left[2]],
                [normal[0], normal[1], normal[2]],
                [0.0, 0.0],
                [up[0], up[1], up[2], 1.0],
            ),
            Vertex::new(
                [up_left[0], up_left[1], up_left[2]],
                [normal[0], normal[1], normal[2]],
                [0.0, 0.0],
                [up[0], up[1], up[2], 1.0],
            ),
            Vertex::new(
                [up_right[0], up_right[1], up_right[2]],
                [normal[0], normal[1], normal[2]],
                [0.0, 0.0],
                [up[0], up[1], up[2], 1.0],
            ),
            Vertex::new(
                [down_right[0], down_right[1], down_right[2]],
                [normal[0], normal[1], normal[2]],
                [0.0, 0.0],
                [up[0], up[1], up[2], 1.0],
            ),
        ]
    }
    let half_side = side_length / 2.0;

    let quad_bottom = quad(
        center + Vector3::new(0.0, -half_side, 0.0),
        Vector3::new(half_side, 0.0, 0.0),
        Vector3::new(0.0, 0.0, -half_side),
    );
    let quad_top = quad(
        center + Vector3::new(0.0, half_side, 0.0),
        Vector3::new(half_side, 0.0, 0.0),
        Vector3::new(0.0, 0.0, half_side),
    );

    let quad_left = quad(
        center + Vector3::new(0.0, 0.0, half_side),
        Vector3::new(half_side, 0.0, 0.0),
        Vector3::new(0.0, -half_side, 0.0),
    );
    let quad_right = quad(
        center + Vector3::new(0.0, 0.0, -half_side),
        Vector3::new(half_side, 0.0, 0.0),
        Vector3::new(0.0, half_side, 0.0),
    );

    let quad_front = quad(
        center + Vector3::new(half_side, 0.0, 0.0),
        Vector3::new(0.0, half_side, 0.0),
        Vector3::new(0.0, 0.0, -half_side),
    );
    let quad_back = quad(
        center + Vector3::new(-half_side, 0.0, 0.0),
        Vector3::new(0.0, half_side, 0.0),
        Vector3::new(0.0, 0.0, half_side),
    );

    (
        vec![
            0,
            2,
            1,
            2,
            0,
            3,
            0 + 4,
            2 + 4,
            1 + 4,
            2 + 4,
            0 + 4,
            3 + 4,
            0 + 8,
            2 + 8,
            1 + 8,
            2 + 8,
            0 + 8,
            3 + 8,
            0 + 12,
            2 + 12,
            1 + 12,
            2 + 12,
            0 + 12,
            3 + 12,
            0 + 16,
            2 + 16,
            1 + 16,
            2 + 16,
            0 + 16,
            3 + 16,
            0 + 20,
            2 + 20,
            1 + 20,
            2 + 20,
            0 + 20,
            3 + 20,
        ],
        vec![
            quad_bottom[0],
            quad_bottom[1],
            quad_bottom[2],
            quad_bottom[3],
            quad_top[0],
            quad_top[1],
            quad_top[2],
            quad_top[3],
            quad_left[0],
            quad_left[1],
            quad_left[2],
            quad_left[3],
            quad_right[0],
            quad_right[1],
            quad_right[2],
            quad_right[3],
            quad_front[0],
            quad_front[1],
            quad_front[2],
            quad_front[3],
            quad_back[0],
            quad_back[1],
            quad_back[2],
            quad_back[3],
        ],
        PrimitiveParameters::new(
            Matrix4::one(),
            albedo,
            metalness,
            roughness,
            Vector3::new(0.0, 0.0, 0.0),
            1.0,
            casts_shadows,
            receives_shadows,
            false,
            false,
            false,
            false,
            false,
            false,
            false,
        ),
    )
}

/// Generate quad. Pass the result into ShapeList constructors, e.g. from_shape().
pub fn generate_quad(
    center: Point3<f32>,
    up: Vector3<f32>,
    right: Vector3<f32>,
    albedo: Vector4<f32>,
    metalness: f32,
    roughness: f32,
    casts_shadows: bool,
    receives_shadows: bool,
) -> (Vec<u32>, Vec<Vertex>, PrimitiveParameters, Vec<Texture2D>) {
    let down_left = center + (-up) + (-right);
    let up_left = center + up + (-right);
    let up_right = center + up + right;
    let down_right = center + (-up) + right;

    let normal = right.normalize().cross(up.normalize()).normalize();

    let vertices = vec![
        Vertex::new(
            [down_left[0], down_left[1], down_left[2]],
            [normal[0], normal[1], normal[2]],
            [0.0, 0.0],
            [up[0], up[1], up[2], 1.0],
        ),
        Vertex::new(
            [up_left[0], up_left[1], up_left[2]],
            [normal[0], normal[1], normal[2]],
            [0.0, 0.0],
            [up[0], up[1], up[2], 1.0],
        ),
        Vertex::new(
            [up_right[0], up_right[1], up_right[2]],
            [normal[0], normal[1], normal[2]],
            [0.0, 0.0],
            [up[0], up[1], up[2], 1.0],
        ),
        Vertex::new(
            [down_right[0], down_right[1], down_right[2]],
            [normal[0], normal[1], normal[2]],
            [0.0, 0.0],
            [up[0], up[1], up[2], 1.0],
        ),
    ];
    let primitive_parameters = PrimitiveParameters::new(
        Matrix4::one(),
        albedo,
        metalness,
        roughness,
        Vector3::new(0.0, 0.0, 0.0),
        1.0,
        casts_shadows,
        receives_shadows,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
    );
    let indices: Vec<u32> = vec![0, 2, 1, 2, 0, 3];

    (indices, vertices, primitive_parameters, vec![])
}
