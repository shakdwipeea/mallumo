mod errors {
    error_chain! {
        errors {
        }
    }
}

mod atmosphere;
pub use self::atmosphere::*;

mod constants;
