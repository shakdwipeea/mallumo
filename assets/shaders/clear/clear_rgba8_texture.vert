layout(rgba8, binding = 0) uniform writeonly image3D input_texture;

void main()
{
    ivec3 loc = ivec3(
        gl_VertexID % imageSize(input_texture).x,
        (gl_VertexID / imageSize(input_texture).x) % imageSize(input_texture).y,
        (gl_VertexID / (imageSize(input_texture).x * imageSize(input_texture).y)) % imageSize(input_texture).z
    );

    imageStore(input_texture, loc, vec4(0.0));
}