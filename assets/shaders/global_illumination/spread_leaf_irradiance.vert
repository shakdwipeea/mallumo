// version.glsl 7
// utility.glsl 60

const bool count_empty_voxels = false; 

// Brick pool data
layout(r32ui, binding = 0) uniform coherent volatile uimage3D brickpool;

void interpolate_values(layout(r32ui) coherent volatile uimage3D brickpool, in ivec3 brick_address);

void main() {
  const uint brickpool_size = imageSize(brickpool).x / 3;

  ivec3 brick_address = ivec3(
      3 * (gl_VertexID % brickpool_size),
      3 * ((gl_VertexID / brickpool_size) % brickpool_size),
      3 * ((gl_VertexID / brickpool_size / brickpool_size) % brickpool_size)
  );

  //imageStore(brickpool, brick_address, uvec4(packUnorm4x8(vec4(1.0, 1.0, 1.0, 1.0)), 0, 0, 0));
  interpolate_values(brickpool, brick_address);
}

vec4 interpolate_4(vec4 value1, vec4 value2, vec4 value3, vec4 value4) {
    vec4 values[4] = {
        value1, value2, value3, value4
    };
    vec4 col = vec4(0);
    int count = 0;
    for(int i = 0; i < 4; ++i) {
        if(values[i].w != 0.0) {
            col += values[i];
            count += 1;
        }
    }
    col /= float(count);

    return col;
}

vec4 interpolate_2(vec4 value1, vec4 value2) {
    vec4 values[2] = {
        value1, value2
    };
    vec4 col = vec4(0);
    int count = 0;
    for(int i = 0; i < 2; ++i) {
        if(values[i].w != 0.0) {
            col += values[i];
            count += 1;
        }
    }
    col /= float(count);

    return col;
}

void interpolate_values(layout(r32ui) coherent volatile uimage3D brickpool, in ivec3 brick_address) {
  vec4 values[8];
  for(int i = 0; i < 8; ++i) {
      values[i] = 
          unpackUnorm4x8(
              imageLoad(brickpool, brick_address + 2 * ivec3(child_offsets[i])).x
            );
  }

  memoryBarrier();
  
  vec4 col = vec4(0);
  
  if (count_empty_voxels == true) {
    // Center
    for (int i = 0; i < 8; ++i) {
        col += 0.125 * values[i];
    }

    imageStore(brickpool, brick_address + ivec3(1,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));


    // Face X
    col = vec4(0);
    col += 0.25 * values[1];
    col += 0.25 * values[3];
    col += 0.25 * values[5];
    col += 0.25 * values[7];
    imageStore(brickpool, brick_address + ivec3(2,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face X Neg
    col = vec4(0);
    col += 0.25 * values[0];
    col += 0.25 * values[2];
    col += 0.25 * values[4];
    col += 0.25 * values[6];
    imageStore(brickpool, brick_address + ivec3(0,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));


    // Face Y
    col = vec4(0);
    col += 0.25 * values[2];
    col += 0.25 * values[3];
    col += 0.25 * values[6];
    col += 0.25 * values[7];
    imageStore(brickpool, brick_address + ivec3(1,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face Y Neg
    col = vec4(0);
    col += 0.25 * values[0];
    col += 0.25 * values[1];
    col += 0.25 * values[4];
    col += 0.25 * values[5];
    imageStore(brickpool, brick_address + ivec3(1,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    
    // Face Z
    col = vec4(0);
    col += 0.25 * values[4];
    col += 0.25 * values[5];
    col += 0.25 * values[6];
    col += 0.25 * values[7];
    imageStore(brickpool, brick_address + ivec3(1,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face Z Neg
    col = vec4(0);
    col += 0.25 * values[0];
    col += 0.25 * values[1];
    col += 0.25 * values[2];
    col += 0.25 * values[3];
    imageStore(brickpool, brick_address + ivec3(1,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));


    // Edges
    col = vec4(0);
    col += 0.5 * values[0];
    col += 0.5 * values[1];
    imageStore(brickpool, brick_address + ivec3(1,0,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[0];
    col += 0.5 * values[2];
    imageStore(brickpool, brick_address + ivec3(0,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[2];
    col += 0.5 * values[3];
    imageStore(brickpool, brick_address + ivec3(1,2,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[3];
    col += 0.5 * values[1];
    imageStore(brickpool, brick_address + ivec3(2,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[0];
    col += 0.5 * values[4];
    imageStore(brickpool, brick_address + ivec3(0,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[2];
    col += 0.5 * values[6];
    imageStore(brickpool, brick_address + ivec3(0,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[3];
    col += 0.5 * values[7];
    imageStore(brickpool, brick_address + ivec3(2,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[1];
    col += 0.5 * values[5];
    imageStore(brickpool, brick_address + ivec3(2,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[4];
    col += 0.5 * values[6];
    imageStore(brickpool, brick_address + ivec3(0,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[6];
    col += 0.5 * values[7];
    imageStore(brickpool, brick_address + ivec3(1,2,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[5];
    col += 0.5 * values[7];
    imageStore(brickpool, brick_address + ivec3(2,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = vec4(0);
    col += 0.5 * values[4];
    col += 0.5 * values[5];
    imageStore(brickpool, brick_address + ivec3(1,0,2), uvec4(packUnorm4x8(col), 0, 0, 0));
  } else {
    // Center
    int count = 0;
    for (int i = 0; i < 8; ++i) {
        if(values[i] != vec4(0.0, 0.0, 0.0, 0.0)) {
            col += values[i];
            count += 1;
        }
    }
    col /= float(count);

    imageStore(brickpool, brick_address + ivec3(1,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face X
    col = interpolate_4(values[1], values[3], values[5], values[7]);
    imageStore(brickpool, brick_address + ivec3(2,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face X Neg
    col = interpolate_4(values[0], values[2], values[4], values[6]);
    imageStore(brickpool, brick_address + ivec3(0,1,1), uvec4(packUnorm4x8(col), 0, 0, 0));


    // Face Y
    col = interpolate_4(values[2], values[3], values[6], values[7]);
    imageStore(brickpool, brick_address + ivec3(1,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face Y Neg
    col = interpolate_4(values[0], values[1], values[4], values[5]);
    imageStore(brickpool, brick_address + ivec3(1,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    
    // Face Z
    col = interpolate_4(values[4], values[5], values[6], values[7]);
    imageStore(brickpool, brick_address + ivec3(1,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    // Face Z Neg
    col = interpolate_4(values[0], values[1], values[2], values[3]);
    imageStore(brickpool, brick_address + ivec3(1,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));


    // Edges
    col = interpolate_2(values[0], values[1]);
    imageStore(brickpool, brick_address + ivec3(1,0,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[0], values[2]);
    imageStore(brickpool, brick_address + ivec3(0,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[2], values[3]);
    imageStore(brickpool, brick_address + ivec3(1,2,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[3], values[1]);
    imageStore(brickpool, brick_address + ivec3(2,1,0), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[0], values[4]);
    imageStore(brickpool, brick_address + ivec3(0,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[2], values[6]);
    imageStore(brickpool, brick_address + ivec3(0,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[3], values[7]);
    imageStore(brickpool, brick_address + ivec3(2,2,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[1], values[5]);
    imageStore(brickpool, brick_address + ivec3(2,0,1), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[4], values[6]);
    imageStore(brickpool, brick_address + ivec3(0,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[6], values[7]);
    imageStore(brickpool, brick_address + ivec3(1,2,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[5], values[7]);
    imageStore(brickpool, brick_address + ivec3(2,1,2), uvec4(packUnorm4x8(col), 0, 0, 0));

    col = interpolate_2(values[4], values[5]);
    imageStore(brickpool, brick_address + ivec3(1,0,2), uvec4(packUnorm4x8(col), 0, 0, 0));
  }
}