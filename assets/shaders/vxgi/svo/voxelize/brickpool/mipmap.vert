#version 450 core

#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 5) buffer NodePoolNext
{
    uint nodepool_next[];
};

layout(std430, binding = 6) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(rgba8, binding = 0) uniform coherent volatile image3D brickpool;

const float gaussianWeight[4] = {0.25, 0.125, 0.0625, 0.03125};

uint[8] load_child_tile(in uint tile_address) {
  uint nodes[8];

  for (int i = 0; i < 8; ++i) {
    nodes[i] = tile_address + i;
  }
  memoryBarrier();

  return nodes;
}

vec4 get_color(in uint nodes[8], in ivec3 position) {
  ivec3 child_position = ivec3(round(vec3(position) / 4.0));
  int child_index = child_position.x + 2 * child_position.y + 4 * child_position.z;

  ivec3 local_position = position - 2 * child_position;

  const uint brickpool_size = imageSize(brickpool).x / 3;
  ivec3 child_brick_address = ivec3(
      3 * (nodes[child_index] % brickpool_size),
      3 * ((nodes[child_index] / brickpool_size) % brickpool_size),
      3 * (((nodes[child_index] / brickpool_size) / brickpool_size) % brickpool_size)
  );

  return imageLoad(brickpool, child_brick_address + local_position);
}

vec4 mipmap_isotropic(in uint nodes[8], in ivec3 pos) {
  vec4 col = vec4(0);
  float weightSum = 0.0;
  
  for (int x = -1; x <= 1; ++x) {
    for (int y = -1; y <= 1; ++y) {
      for (int z = -1; z <= 1; ++z) {
        const ivec3 lookupPos = pos + ivec3(x, y, z);

        if (lookupPos.x >= 0 && lookupPos.y >= 0 && lookupPos.z >= 0 &&
            lookupPos.x <= 4 && lookupPos.y <= 4 && lookupPos.z <= 4)
        {
          const int manhattanDist = abs(x) + abs(y) + abs(z);
          const float weight = gaussianWeight[manhattanDist];
          const vec4 lookupColor = get_color(nodes, lookupPos);

          col += weight * lookupColor;
          weightSum += weight;
        }

      }
    }
  }

  return col / weightSum;
}

void main() {
  const uint node_address = gl_VertexID;
  const int  node_level = int(nodepoint_positions[node_address].w);

  if (node_level == level) {
    const uint brickpool_size = imageSize(brickpool).x / 3;
    const ivec3 brick_address = ivec3(
        3 * (node_address % brickpool_size),
        3 * ((node_address / brickpool_size) % brickpool_size),
        3 * (((node_address / brickpool_size) / brickpool_size) % brickpool_size)
    );

    uint node_next = nodepool_next[node_address] & NODE_MASK_VALUE;

    if (node_next == 0) { 
      return;
    }

    uint[8] child_tiles = load_child_tile(node_next);
    
    // Center
    vec4 color = mipmap_isotropic(child_tiles, ivec3(2, 2, 2));
    imageStore(brickpool, brick_address + ivec3(1,1,1), color);

    // Faces
    vec4 left = mipmap_isotropic(child_tiles, ivec3(0, 2, 2));
    vec4 right = mipmap_isotropic(child_tiles, ivec3(4, 2, 2));
    vec4 bottom = mipmap_isotropic(child_tiles, ivec3(2, 0, 2));
    vec4 top = mipmap_isotropic(child_tiles, ivec3(2, 4, 2));
    vec4 near = mipmap_isotropic(child_tiles, ivec3(2, 2, 0));
    vec4 far = mipmap_isotropic(child_tiles, ivec3(2, 2, 4));

    imageStore(brickpool, brick_address + ivec3(0,1,1), left);
    imageStore(brickpool, brick_address + ivec3(2,1,1), right);
    imageStore(brickpool, brick_address + ivec3(1,0,1), bottom);
    imageStore(brickpool, brick_address + ivec3(1,2,1), top);
    imageStore(brickpool, brick_address + ivec3(1,1,0), near);
    imageStore(brickpool, brick_address + ivec3(1,1,2), far);

    // Edges
    vec4 nearBottom = mipmap_isotropic(child_tiles, ivec3(2, 0, 0));
    vec4 nearRight = mipmap_isotropic(child_tiles, ivec3(4, 2, 0));
    vec4 nearTop = mipmap_isotropic(child_tiles, ivec3(2, 4, 0));
    vec4 nearLeft = mipmap_isotropic(child_tiles, ivec3(0, 2, 0));
    vec4 farBottom = mipmap_isotropic(child_tiles, ivec3(2, 0, 4));
    vec4 farRight = mipmap_isotropic(child_tiles, ivec3(4, 2, 4));
    vec4 farTop = mipmap_isotropic(child_tiles, ivec3(2, 4, 4));
    vec4 farLeft = mipmap_isotropic(child_tiles, ivec3(0, 2, 4));
    vec4 leftBottom = mipmap_isotropic(child_tiles, ivec3(0, 0, 2));
    vec4 leftTop = mipmap_isotropic(child_tiles, ivec3(0, 4, 2));
    vec4 rightBottom = mipmap_isotropic(child_tiles, ivec3(4, 0, 2));
    vec4 rightTop = mipmap_isotropic(child_tiles, ivec3(4, 4, 2));
    
    imageStore(brickpool, brick_address + ivec3(1,0,0), nearBottom);
    imageStore(brickpool, brick_address + ivec3(2,1,0), nearRight);
    imageStore(brickpool, brick_address + ivec3(1,2,0), nearTop);
    imageStore(brickpool, brick_address + ivec3(0,1,0), nearLeft);
    imageStore(brickpool, brick_address + ivec3(1,0,2), farBottom);
    imageStore(brickpool, brick_address + ivec3(2,1,2), farRight);
    imageStore(brickpool, brick_address + ivec3(1,2,2), farTop);
    imageStore(brickpool, brick_address + ivec3(0,1,2), farLeft);
    imageStore(brickpool, brick_address + ivec3(0, 0, 1), leftBottom);
    imageStore(brickpool, brick_address + ivec3(2, 0, 1), rightBottom);
    imageStore(brickpool, brick_address + ivec3(0, 2, 1), leftTop);
    imageStore(brickpool, brick_address + ivec3(2, 2, 1), rightTop);

    // corners
    vec4 nearRightTop = mipmap_isotropic(child_tiles, ivec3(4, 4, 0));
    vec4 nearRightBottom = mipmap_isotropic(child_tiles, ivec3(4, 0, 0));
    vec4 nearLeftTop = mipmap_isotropic(child_tiles, ivec3(0, 4, 0));
    vec4 nearLeftBottom = mipmap_isotropic(child_tiles, ivec3(0, 0, 0));
    vec4 farRightTop = mipmap_isotropic(child_tiles, ivec3(4, 4, 4));
    vec4 farRightBottom = mipmap_isotropic(child_tiles, ivec3(4, 0, 4));
    vec4 farLeftTop = mipmap_isotropic(child_tiles, ivec3(0, 4, 4));
    vec4 farLeftBottom = mipmap_isotropic(child_tiles, ivec3(0, 0, 4));
    
    imageStore(brickpool, brick_address + ivec3(2, 2, 0), nearRightTop);
    imageStore(brickpool, brick_address + ivec3(2, 0, 0), nearRightBottom);
    imageStore(brickpool, brick_address + ivec3(0, 2, 0), nearLeftTop);
    imageStore(brickpool, brick_address + ivec3(0, 0, 0), nearLeftBottom);
    imageStore(brickpool, brick_address + ivec3(2, 2, 2), farRightTop);
    imageStore(brickpool, brick_address + ivec3(2, 0, 2), farRightBottom);
    imageStore(brickpool, brick_address + ivec3(0, 2, 2), farLeftTop);
    imageStore(brickpool, brick_address + ivec3(0, 0, 2), farLeftBottom);
  }
}
