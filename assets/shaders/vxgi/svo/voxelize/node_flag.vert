#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer VoxelPositions
{
    uint voxel_positions[];
};

layout(std430, binding = 1) buffer NodePoolNext
{
    uint nodepool_next[];
};

layout(std430, binding = 2) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

int traverse_octree(in uint levels, 
                    in vec3 voxel_position, 
                    out uint found_on_level) 
{
  vec3 node_position = vec3(0.0);
  vec3 node_position_max = vec3(1.0);

  int node_address = 0;
  found_on_level = 0;
  float side_length = 1.0;
  
  for (uint level = 0; level < levels; ++level) {
    uint node = nodepool_next[node_address];

    // if current node points to *null* (0) nodepool
    uint child_start_address = node & NODE_MASK_VALUE;
    if (child_start_address == 0U) {
        found_on_level = level;
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    node_address = int(child_start_address + offset);
    node_position += vec3(child_offsets[offset]) * vec3(side_length);
    node_position_max = node_position + vec3(side_length);

    side_length = side_length / 2.0;
    voxel_position = 2.0 * voxel_position - vec3(offset_vec);
  } 

  return node_address;
}

void main() {
  uint  voxel_position_encoded = voxel_positions[gl_VertexID];
  uvec3 voxel_position_uint = uint_to_uvec3(voxel_position_encoded);
  vec3  voxel_position_float = vec3(voxel_position_uint) / vec3(vxgi_options.dimension); 

  uint found_on_level = 0;
  int node_address = traverse_octree(vxgi_options.levels, 
                                     voxel_position_float, 
                                     found_on_level);

  if (found_on_level < vxgi_options.levels - 1) {
    uint node = nodepool_next[node_address];
    
    if (nodepoint_positions[node_address].w == level) {
      if(uint(node & NODE_MASK_VALUE) == uint(0)) {
        nodepool_next[node_address] = NODE_MASK_TAG | node;
      }
    }
  }
}



