#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define INDICES_BINDING 1
#define VERTICES_BINDING 2
#define PRIMITIVE_PARAMETERS_BINDING 3

#include vxgi/options.glsl
#include libs/vertices.glsl
#include libs/parameters.glsl

layout(location = 0) out vec3 vertex_world_position;
layout(location = 1) out vec3 vertex_world_normal;

void main(void) {
  int index = indices[gl_VertexID];

  vec3 position = vertices[index].position.xyz;
  vec3 normal = vertices[index].normal.xyz;

  mat4 model_matrix = primitive_parameters.model_matrix;
  mat3 normal_matrix = transpose(inverse(mat3(model_matrix)));

  vertex_world_position = vec3(model_matrix * vec4(position, 1.0));
  vertex_world_normal = normalize(normal_matrix * normal);

  gl_Position = vec4(vertex_world_position, 1.0f);
}