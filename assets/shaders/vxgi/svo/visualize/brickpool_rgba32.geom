#version 450

layout (points) in;
layout (triangle_strip, max_vertices=14) out;

#define CAMERA_BINDING 0

#include libs/camera.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

in float side_length[];
in ivec3 brick_address_vertex[];

out flat ivec3 brick_address_geometry;

const vec3 triangle_offsets[14] = {
  vec3(-1.0, 1.0, -1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(1.0, -1.0, -1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(1.0, 1.0, -1.0),
  vec3(1.0, 1.0, 1.0),
  vec3(-1.0, 1.0, -1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(-1.0, -1.0, -1.0),
  vec3(-1.0, -1.0, 1.0),
  vec3(1.0, -1.0, 1.0),
  vec3(-1.0, 1.0, 1.0),
  vec3(1.0, 1.0, 1.0),
};

void main()
{
    for(int i = 0; i < triangle_offsets.length(); i++)
    {
        brick_address_geometry = brick_address_vertex[0];
        vec3 world_position = vec3(gl_in[0].gl_Position) + triangle_offsets[i] * (side_length[0] * (1.0/6.0));
        gl_Position = camera.projection_view_matrix * vec4(world_position, 1.0);
        EmitVertex();
    }
    EndPrimitive();
}  