#version 450 core

layout(location = 0) in flat uint index;

layout(std430, binding = 1) buffer VoxelAlbedos
{
    uint voxel_albedos[];
};

layout(location = 0) out vec4 color;

void main() {
    vec3 voxel_albedo = unpackUnorm4x8(voxel_albedos[index]).rgb;

    color = vec4(voxel_albedo, 1.0);
}