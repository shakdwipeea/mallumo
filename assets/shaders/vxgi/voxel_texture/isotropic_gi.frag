#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1
#define SUN_BINDING 2
#define SUN_SHADOWMAP_BINDING 8

#include libs/consts.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl

#include libs/camera.glsl
#include sun/sun.glsl
#include vxgi/options.glsl

const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};

void correct_alpha(inout vec4 color, in float alpha_correction) {
    float color_old = color.a;
    color.a = 1.0 - pow(1.0 - color.a, alpha_correction);
    color.xyz = color.xyz * (color.a / clamp(color_old, 0.0001, 10000.0));
}

// Volume textures
// Maximum 9 levels for voxel texture
layout(binding = 9) uniform sampler3D volume_radiance[10];
layout(binding = 19) uniform sampler3D volume_emission[10];

vec4 vxgi_sample_function(vec3 position, vec3 weight, uvec3 face, float lod, VoxelTextureType type) {
  lod = clamp(lod, 0.0f, float(vxgi_options.levels));
  
  int base_lod = int(floor(lod));
  int mip_lod = int(ceil(lod));

  vec4 base_value = vec4(0.0);
  vec4 mip_value = vec4(0.0);

  if (type == VoxelTextureTypeRadiance) {
    base_value = texture(volume_radiance[base_lod], position);
    mip_value = texture(volume_radiance[mip_lod], position);
  } 
  else if (type == VoxelTextureTypeEmission) {
    base_value = texture(volume_emission[base_lod], position);
    mip_value = texture(volume_emission[mip_lod], position);
  }

  // Alpha correct
  const float alpha_correction = float(pow2[base_lod + 1]) / float(pow2[vxgi_options.levels]);

  correct_alpha(base_value, alpha_correction);
  correct_alpha(mip_value, alpha_correction * 2);

  vec4 result = mix(base_value, mip_value, lod - base_lod);

  return result;
}

#include vxgi/gi_shared.glsl