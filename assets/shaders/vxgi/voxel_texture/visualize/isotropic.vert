#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout(location = 2) uniform uint dimension;
layout(location = 3) uniform uint level;

layout(location = 0) out ivec3 vertex_voxel_position;

void main() {
    float voxel_size = 1.0 / float(dimension);
    float voxel_half_size = voxel_size / 2.0;

    ivec3 voxel_position = ivec3(
        gl_VertexID % dimension,
        (gl_VertexID / dimension) % dimension,
        (gl_VertexID / dimension / dimension) % dimension
     );

    vec3 position = vec3(voxel_position) * voxel_size + vec3(voxel_half_size);

    vertex_voxel_position = voxel_position;
    gl_Position = vec4(position, 1.0);
}