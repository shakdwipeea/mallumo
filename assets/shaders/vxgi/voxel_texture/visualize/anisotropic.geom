#version 450 core

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

layout (points) in;
layout (triangle_strip, max_vertices = 24) out;

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

layout(location = 0) in  flat ivec3 vertex_voxel_position[];

layout(location = 0) out flat  vec4 geometry_voxel_color;

layout(location = 0) uniform uint dimension;
layout(location = 1) uniform uint type;
layout(location = 2) uniform uint level;
layout(binding = 0) uniform sampler3D texture[6];

// X-, X+, Y-, Y+, Z-, Z+

const vec4 cube_vertices[8] = vec4[8](
    vec4(-1.0, -1.0, -1.0, 1.0), // 0: back bottom left
    vec4(-1.0,  1.0, -1.0, 1.0), // 1: back top    left
    vec4( 1.0, -1.0, -1.0, 1.0), // 2: back bottom right
    vec4( 1.0,  1.0, -1.0, 1.0), // 3: back top    right

    vec4(-1.0, -1.0, 1.0, 1.0),  // 4: front bottom left
    vec4(-1.0,  1.0, 1.0, 1.0),  // 5: front top    left
    vec4( 1.0, -1.0, 1.0, 1.0),  // 6: front bottom right
    vec4( 1.0,  1.0, 1.0, 1.0)   // 7: front top    right
);

const int cube_indices[24]  = int[24](
    1,0,5,4, // left   X-
    7,6,3,2, // right  X+

    4,0,6,2, // bottom Y-
    3,1,7,5, // top    Y+

    0,1,2,3, // front  Z-
    7,5,6,4  // back   Z+
);

const vec4 colors[6] = vec4[6](
    vec4(1.0, 0.0, 0.0, 1.0), // X- 
    vec4(0.0, 1.0, 0.0, 1.0), // X+
    vec4(0.0, 0.0, 1.0, 1.0), // Y-
    vec4(1.0, 0.0, 0.0, 1.0), // Y+
    vec4(0.0, 1.0, 0.0, 1.0), // Z-
    vec4(0.0, 0.0, 1.0, 1.0)  // Z+
);

void main()
{
    vec3 position = gl_in[0].gl_Position.xyz * 2.0 - 1.0;
    float edge_size = 1.0 / float(dimension);

    for (int index = 0; index < 24; index++) {
      vec3 vertex = cube_vertices[cube_indices[index]].rgb;

      if (level == 0) {
        geometry_voxel_color = texelFetch(texture[0], vertex_voxel_position[0], 0);
      } else {
          int mip_level = int(level) - int(1);
          geometry_voxel_color = texelFetch(texture[index / 4], vertex_voxel_position[0], mip_level);
          //geometry_voxel_color = colors[index / 4];
      }

      gl_Position = camera.projection_view_matrix * vec4(position + vertex * edge_size, 1.0);

      EmitVertex();

      if ((index + 1) % 4 == 0) {
        EndPrimitive();
      }
    }
}  