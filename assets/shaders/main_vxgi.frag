#version 450 core

#define MIPMAP_HARDCAP 5.4f /* Too high mipmap levels => glitchiness, too low mipmap levels => sharpness. */

#extension GL_ARB_bindless_texture : enable

const float PI = 3.14159265359;
const float SQRT2 = 1.41421356237;
const float ISQRT2 = 0.70710678118;

struct PrimitiveParameter {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness;             // 16B
  vec4 emission;                       // 16B
  
  uvec2 albedo_texture;                // 8B
  uvec2 metallic_roughness_texture;    // 8B
  uvec2 occlusion_texture;             // 8B
  uvec2 normal_texture;                // 8B
  uvec2 emissive_texture;              // 8B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  // Image based lighting
  uvec2 diffuse_ibl;                   // 8B
  uvec2 specular_ibl;                  // 8B

  // Padding
  uvec4 padding;                       //+16B == 192 (64*3)
};

struct Light {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std140, binding = 1) uniform Globals {
    uint levels;
    float voxel_size;
} globals;

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(std430, binding = 0) buffer PrimitiveParameters
{
    PrimitiveParameter parameters[];
} primitive_parameters;

layout(std430, binding = 3) buffer LightParameters
{
    Light lights[];
} light_parameters;

layout(std430, binding = 4) buffer PhysicalParameters
{
    float refractive_indeces[];
} physical_parameters;

in vertexOut {
  mat3 TBN;
  vec3 world_position;  
  vec2 texture_coordinate;
  vec3 normal;
  flat int draw_id;
} vertex_out;

layout(binding = 0) uniform sampler3D voxel_color_texture;
layout(binding = 1) uniform sampler3D voxel_alpha_texture;

layout(binding = 2) uniform samplerCubeArray shadowmaps[16];

layout (location = 0) out vec4 color;

///
/// Normal Distribution Function
///
///                                   α^2
/// Trowbridge-Reitz GGX = ----------------------------
///                        π( (n⋅h)^2 * (α^2 − 1) + 1)^2
///
/// N - normal vector 
/// H - half-way vector
/// a - α = roughness
///
float distributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float nom    = a2;
    float denom  = (NdotH2 * (a2 - 1.0) + 1.0);
    denom        = PI * denom * denom;
	
    return nom / denom;
}

/// 
/// Geometry Function
/// 
///                    n⋅v
/// SchlickGGX = ---------------
///                (n⋅v)(1−k)+k
/// 
/// k - remapping of α based on whether one uses direct or IBL lighting
///     for direct k = (α + 1)^2 / 8
///     for IBL    k = (α^2) / 2
/// N ⋅ V
///
float geometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

/// 
/// Smith's method for taking into account the view direction(geometry obstruction) 
/// and the light direction vector(geometry shadowing)
/// 
/// G(n, v, l, k) = SchlickGGX(n, v, k) * SchlickGGX(n, l, k)
/// 
float geometrySmith(vec3 N, vec3 V, vec3 L, float k)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx1 = geometrySchlickGGX(NdotV, k);
    float ggx2 = geometrySchlickGGX(NdotL, k);
	
    return ggx1 * ggx2;
}

///
/// Fresnel-Schlick approximation
///
/// f0 - base reflectivity of the surface
/// N ⋅ V
///
vec3 fresnelSchlick(float NdotV, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - NdotV, 5.0);
}

vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(1.0 - cosTheta, 5.0);
}   

///
/// Shadow calculations
///
vec3 grid_sampling_disk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1), 
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

/*
float in_shadow(vec3 frag_pos, Light light, uint shadowmap_index)
{
    vec3 frag_to_light = frag_pos - light.pos_far.xyz;
    float closest_depth = texture(shadowmaps[shadowmap_index], vec4(frag_to_light, 0.0)).r;
    closest_depth *= light.pos_far.w;
    float current_depth = length(frag_to_light);
    float bias = 0.005;
    float shadow = current_depth -  bias > closest_depth ? 1.0 : 0.0;

    return shadow;
} */

float in_shadow(vec3 frag_pos, Light light, uint shadowmap_index)
{
    // Get vector between fragment position and light position
    vec3 frag_to_light = frag_pos - light.pos_far.xyz;
    // Get current linear depth as the length between the fragment and light position
    float current_depth = length(frag_to_light);
    // Test for shadows with PCF
    float shadow = 0.0;
    int samples = 20;
    float view_distance = length(camera.position.xyz - frag_pos);
    float bias = 0.005;
    float disk_radius = (1.0 + (view_distance / light.pos_far.w)) / 700.0;
    for(int i = 0; i < samples; ++i)
    {
        float closest_depth = texture(shadowmaps[shadowmap_index], vec4(frag_to_light + grid_sampling_disk[i] * disk_radius, 0.0)).r;
        closest_depth *= light.pos_far.w;   // Undo mapping [0;1]
        if(abs(current_depth - bias) > closest_depth)
            shadow += 1.0;
    }
    shadow /= float(samples);
    
    return shadow;
}

// Returns a vector that is orthogonal to u.
vec3 orthogonal(vec3 u){
	u = normalize(u);
	vec3 v = vec3(0.99146, 0.11664, 0.05832); // Pick any normalized vector.
	return abs(dot(u, v)) > 0.99999f ? cross(u, vec3(0, 1, 0)) : cross(u, v);
}

// Scales and bias a given vector (i.e. from [-1, 1] to [0, 1]).
vec3 scaleAndBias(const vec3 p) { return 0.5f * p + vec3(0.5f); }

// Returns true if the point p is inside the unity cube. 
bool isInsideCube(const vec3 p, float e) { return abs(p.x) < 1 + e && abs(p.y) < 1 + e && abs(p.z) < 1 + e; }

// Returns a soft shadow blend by using shadow cone tracing.
// Uses 2 samples per step, so it's pretty expensive.
float traceShadowCone(vec3 from, vec3 direction, float targetDistance, vec3 normal){
	from += normal * 0.05f; // Removes artifacts but makes self shadowing for dense meshes meh.

	float acc = 0;

	float dist = 3 * globals.voxel_size;
	// I'm using a pretty big margin here since I use an emissive light ball with a pretty big radius in my demo scenes.
	const float STOP = targetDistance - 16 * globals.voxel_size;

	while(dist < STOP && acc < 1){	
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0)) break;
		c = scaleAndBias(c);
		float l = pow(dist, 2); // Experimenting with inverse square falloff for shadows.
		float s1 = 0.062 * textureLod(voxel_alpha_texture, c, 1 + 0.75 * l).r;
		float s2 = 0.135 * textureLod(voxel_alpha_texture, c, 4.5 * l).r;
		float s = s1 + s2;
		acc += (1 - acc) * s;
		dist += 0.9 * globals.voxel_size * (1 + 0.05 * l);
	}
	return pow(smoothstep(0, 1, acc * 1.4), 1.0 / 1.4);
}

// Traces a specular voxel cone.
vec3 traceSpecularVoxelCone(vec3 from, vec3 direction, vec3 normal, float max_distance, float kS){
	direction = normalize(direction);

	const float OFFSET = 8 * globals.voxel_size;
	const float STEP = globals.voxel_size;

	from += OFFSET * normal;
	
	vec4 acc = vec4(0.0f);
	float dist = OFFSET;

    float factor = max(0.05, 20.95 - 18.75 * kS);

	// Trace.
	while(dist < max_distance && acc.a < 1){ 
		vec3 c = from + dist * direction;
		if(!isInsideCube(c, 0)) break;
		c = scaleAndBias(c); 
		
		float level = 0.1 * factor * log2(1 + dist / globals.voxel_size);
		vec4 voxel = vec4(textureLod(voxel_color_texture, c, min(5.4, level)).rgb, textureLod(voxel_alpha_texture, c, min(5.4, level)).r);
		float f = 1 - acc.a;
		acc.rgb += 0.25 * (1 + factor) * voxel.rgb * voxel.a * f;
		acc.a += 0.25 * voxel.a * f;
		dist += STEP * (1.0f + 0.125f * level);
	}
	return 1.0 * pow(factor + 1, 0.8) * acc.rgb;
}

// Traces a diffuse voxel cone.
vec3 traceDiffuseVoxelCone(const vec3 from, vec3 direction){
	direction = normalize(direction);
	
	const float CONE_SPREAD = 0.325;

	vec4 acc = vec4(0.0f);

	// Controls bleeding from close surfaces.
	// Low values look rather bad if using shadow cone tracing.
	// Might be a better choice to use shadow maps and lower this value.
	float dist = 0.1953125;

	// Trace.
	while(dist < SQRT2 && acc.a < 1.0){
		vec3 c = from + dist * direction;
		c = scaleAndBias(from + dist * direction);
		float l = (1 + CONE_SPREAD * dist / globals.voxel_size);
		float level = log2(l);
		float ll = (level + 1) * (level + 1);

		vec4 voxel = vec4(textureLod(voxel_color_texture, c, min(float(globals.levels) - 1.0, level)).rgb, textureLod(voxel_alpha_texture, c, min(float(globals.levels) - 1.0, level)).r);

        acc += 0.075 * ll * voxel * pow(1 - voxel.a, 2);
		dist += ll * globals.voxel_size * 2;
	}
	return pow(acc.rgb * 2.0, vec3(1.5));
}

vec3 indirectDiffuseLight(vec3 frag_pos, vec3 normal) {
	const float ANGLE_MIX = 0.5f; // Angle mix (1.0f => orthogonal direction, 0.0f => direction of normal).

	const float w[3] = {1.0, 1.0, 1.0}; // Cone weights.

	// Find a base for the side cones with the normal as one of its base vectors.
	const vec3 ortho = normalize(orthogonal(normal));
	const vec3 ortho2 = normalize(cross(ortho, normal));

	// Find base vectors for the corner cones too.
	const vec3 corner = 0.5f * (ortho + ortho2);
	const vec3 corner2 = 0.5f * (ortho - ortho2);

	// Find start position of trace (start with a bit of offset).
	const vec3 N_OFFSET = normal * (1 + 4 * ISQRT2) * globals.voxel_size;
	const vec3 C_ORIGIN = frag_pos + N_OFFSET;

	// Accumulate indirect diffuse light.
	vec3 acc = vec3(0);

	// We offset forward in normal direction, and backward in cone direction.
	// Backward in cone direction improves GI, and forward direction removes
	// artifacts.
	const float CONE_OFFSET = -0.001;

	// Trace front cone
    acc += w[0] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * normal, normal);

	// Trace 4 side cones.
	const vec3 s1 = mix(normal, ortho, ANGLE_MIX);
	const vec3 s2 = mix(normal, -ortho, ANGLE_MIX);
	const vec3 s3 = mix(normal, ortho2, ANGLE_MIX);
	const vec3 s4 = mix(normal, -ortho2, ANGLE_MIX);

	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * ortho, s1);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * ortho, s2);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * ortho2, s3);
	acc += w[1] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * ortho2, s4);

	// Trace 4 corner cones.
	const vec3 c1 = mix(normal, corner, ANGLE_MIX);
	const vec3 c2 = mix(normal, -corner, ANGLE_MIX);
	const vec3 c3 = mix(normal, corner2, ANGLE_MIX);
	const vec3 c4 = mix(normal, -corner2, ANGLE_MIX);

	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * corner, c1);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * corner, c2);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN + CONE_OFFSET * corner2, c3);
	acc += w[2] * traceDiffuseVoxelCone(C_ORIGIN - CONE_OFFSET * corner2, c4);

	// Return result.
	return acc;
}

void main() {
    uvec2 albedo_texture = primitive_parameters.parameters[vertex_out.draw_id].albedo_texture;
    uvec2 metallic_roughness_texture = primitive_parameters.parameters[vertex_out.draw_id].metallic_roughness_texture;
    uvec2 occlusion_texture = primitive_parameters.parameters[vertex_out.draw_id].occlusion_texture;
    uvec2 normal_texture = primitive_parameters.parameters[vertex_out.draw_id].normal_texture;
    uvec2 emissive_texture = primitive_parameters.parameters[vertex_out.draw_id].emissive_texture;
    uvec2 diffuse_ibl = primitive_parameters.parameters[vertex_out.draw_id].diffuse_ibl;
    uvec2 specular_ibl = primitive_parameters.parameters[vertex_out.draw_id].specular_ibl;

    sampler2D albedo_sampler = sampler2D(albedo_texture); 
    sampler2D metallic_roughness_sampler = sampler2D(metallic_roughness_texture);
    sampler2D occlusion_sampler = sampler2D(occlusion_texture);
    sampler2D normal_sampler = sampler2D(normal_texture);
    sampler2D emissive_sampler = sampler2D(emissive_texture);

    vec4 albedo = primitive_parameters.parameters[vertex_out.draw_id].albedo;
    if (albedo_texture != uvec2(0, 0)) {
        albedo *= texture(albedo_sampler, vertex_out.texture_coordinate);
    }

    if (albedo.a <= 0.001) {
        discard;
    }

    float metalness = primitive_parameters.parameters[vertex_out.draw_id].metallic_roughness.r;
    float roughness = primitive_parameters.parameters[vertex_out.draw_id].metallic_roughness.g;
    if (metallic_roughness_texture != uvec2(0, 0)) {
        metalness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).r;
        roughness *= texture(metallic_roughness_sampler, vertex_out.texture_coordinate).g;
    }

    float occlusion = 1.0;
    if (occlusion_texture != uvec2(0, 0)) {
        occlusion = texture(occlusion_sampler, vertex_out.texture_coordinate).r;
    }

    vec3 normal = vertex_out.normal;
    if (normal_texture != uvec2(0, 0)) {
        normal = texture(normal_sampler, vertex_out.texture_coordinate).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        normal = vertex_out.TBN * normal; 
    }
    normal = normalize(normal);

    vec3 emission = primitive_parameters.parameters[vertex_out.draw_id].emission.rgb;
    if(emissive_texture != uvec2(0, 0)) {
        emission *= texture(emissive_sampler, vertex_out.texture_coordinate).rgb;
    }

    // Approximation of F0 for Fresnel
    vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);

    vec3 N = normal;
    vec3 V = normalize(vec3(camera.position) - vertex_out.world_position);
    vec3 R = reflect(-V, N); 

    vec3 Lo = vec3(0.0);

    for(uint i = 0; i < light_parameters.lights.length(); i++) {
        vec3 light_position = light_parameters.lights[i].pos_far.xyz;
        vec3 light_color = light_parameters.lights[i].color;
        vec3 light_direction = light_position - vertex_out.world_position;

        vec3 L = normalize(light_direction); 
        vec3 H = normalize(V + L);

        // Radiance
        float distance    = length(light_direction);
        float attenuation = 1.0 / (distance * distance); // inverse-square law
        vec3 radiance     = light_color * attenuation;

        // Final Cook-Torrance BRDF
        //     DFG
        // -----------
        // 4(ωo⋅n)(ωi⋅n)
        float D = distributionGGX(N, H, roughness);
        float G = geometrySmith(N, V, L, roughness);
        vec3  F = fresnelSchlick(max(dot(H, V), 0.0), f0);

        vec3  nominator   = D * G * F;
        float denominator = 4 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.001; 
        vec3  brdf        = nominator / denominator; 

        // kS = energy that gets reflected
        // kD = energy that gets refracted ( 1.0 - reflected )
        vec3 kS = F;
        vec3 kD = vec3(1.0) - kS; 

        // nullify kD if the surface is metallic
        // metallic surface does not refract light
        kD *= 1.0 - metalness;

        // light's outgoing reflectance
        // from reflectance equation
        // (      c              )
        // ( kD * - + kS * BRDF  ) * radiance * N ⋅ L
        // (      π              )
        float NdotL = max(dot(N, L), 0.0);

        //float shadow = light_parameters.lights[i].has_shadowmap == 0
        //               ? 0.0
        //              : in_shadow(vertex_out.world_position, light_parameters.lights[i], i);
        float shadow = traceShadowCone(vertex_out.world_position, light_direction, distance, normal);
        Lo += (1.0 - shadow) * ((kD * albedo.rgb / PI + brdf) * radiance * NdotL);
    }

    // vxgi
    vec3 view_direction = normalize(vertex_out.world_position - camera.position.xyz);

    //float kS = 0.5;
    //float kD = 1.0 - kS;

    float kS = 1.0;//max(metalness, 0.05);
    float kD = 1.0 - kS;

    float max_distance = distance(vec3(abs(vertex_out.world_position)), vec3(-1));
    vec3 ambient = kD * indirectDiffuseLight(vertex_out.world_position, normal) * (albedo.rgb + vec3(0.001f));

    const vec3 reflection = normalize(reflect(view_direction, normal));
	ambient += kS * traceSpecularVoxelCone(vertex_out.world_position, reflection, normal, max_distance, kS) * (albedo.rgb + vec3(0.001f));
    
    ambient *= 5;

    // vec3 ambient = vec3(0.0, 0.0, 0.0);
    // vec3 kS = fresnelSchlickRoughness(max(dot(N, V), 0.0), f0, roughness);
    // vec3 kD = 1.0 - kS;  
    
    // vec3 irradiance = texture(diffuse_sampler, vec4(N, 0)).rgb;
    // vec3 diffuse = irradiance * albedo.rgb;
    // kD *= 1.0 - metalness;
    
    // sample both the pre-filter map and the BRDF lut and combine them together as per the Split-Sum approximation to get the IBL specular part.
    // const float MAX_REFLECTION_LOD = 5.0;
    // vec3 prefiltered_color = textureLod(specular_sampler, vec4(R, 0),  roughness * MAX_REFLECTION_LOD).rgb;    
    // vec2 brdf  = texture(brdf_sampler, vec2(max(dot(N, V), 0.0), roughness)).rg;
    // vec3 specular = prefiltered_color * (kS * brdf.x + brdf.y);

    // ambient = (kD * diffuse + specular) * occlusion;

    //Lo = Lo + ambient;
    
    Lo = Lo + emission;

    // HDR -> LDR using Reinhard operator
    float min_white = 1.5;
    Lo = (Lo * (vec3(1.0) + Lo / vec3(min_white * min_white))) / (vec3(1.0) + Lo);
    
    // Gamma correction
    Lo = pow(Lo, vec3(1.0/2.2));

    color = vec4(ambient, 1.0);
}