layout(rgba32f, binding = 0) uniform writeonly imageCubeArray skybox;

layout(std140, binding = 1) uniform RenderSkyboxGlobals
{
    mat4 view_matrices[6];
    mat4 projection_matrix;
    vec4 position;
} render_skybox_globals;

layout(location = 0) uniform vec3 sun_direction;
layout(binding = 0) uniform sampler2D transmittance_texture;
layout(binding = 1) uniform sampler3D scattering_texture;
layout(binding = 3) uniform sampler2D irradiance_texture;

// screen_position:
// 
// 0.0, 1.0       1.0, 1.0
//
//
// 0.0, 0.0       1.0, 0.0
vec3 ray_direction(vec2 screen_position, int face) {
    vec2 ndc = screen_position * 2.0 - 1.0;

    vec4 ray_clip = vec4(ndc, -1.0, 1.0);
    
    vec4 ray_eye = inverse(render_skybox_globals.projection_matrix) * ray_clip;
    ray_eye = vec4(ray_eye.xy, -1.0, 0.0);

    return normalize((inverse(render_skybox_globals.view_matrices[face]) * ray_eye).xyz);
}

void main() {
    ivec3 skybox_size = imageSize(skybox);

    int x = gl_VertexID % skybox_size.x;
    int y = (gl_VertexID / skybox_size.x) % skybox_size.y;
    int face = gl_VertexID / (skybox_size.x * skybox_size.y);
    ivec3 skybox_coordinate = ivec3(x, y, face);

    vec2 face_coordinate = vec2(
        float(x) / skybox_size.x,
        float(y) / skybox_size.y
    );

    vec3 ray_direction = ray_direction(face_coordinate, face);
    vec3 ray_start = render_skybox_globals.position.xyz;
    vec3 transmittance;
    vec3 luminance = get_sky_luminance(render_skybox_globals.position.xyz, ray_direction, sun_direction, transmittance, transmittance_texture, scattering_texture);

    float sun = get_sun(ray_direction, sun_direction);
    vec3 sun_luminance = sun * globals.parameters.solar_irradiance * globals.parameters.sun_spectral_radiance_to_luminance;
    luminance += transmittance * sun_luminance;

    vec3 color = adjust_exposure(luminance);

    imageStore(skybox, skybox_coordinate, vec4(color, 1.0));
}
