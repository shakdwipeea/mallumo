layout(location = 0) out vec3 delta_multiple_scattering;
layout(location = 1) out vec4 scattering;

layout(binding = 0) uniform sampler2D transmittance_texture;
layout(binding = 1) uniform sampler3D scattering_density_texture;

void main() {
    vec3 frag_coord = vec3(gl_FragCoord.xy, globals.parameters.layer + 0.5);
    float frag_coord_nu = floor(frag_coord.x / SCATTERING_TEXTURE_MU_S_SIZE);
    float frag_coord_mu_s = mod(frag_coord.x, SCATTERING_TEXTURE_MU_S_SIZE);

    vec4 uvwz = vec4(frag_coord_nu, frag_coord_mu_s, frag_coord.y, frag_coord.z) / SCATTERING_TEXTURE_SIZE;
    float H = sqrt(globals.parameters.top_radius * globals.parameters.top_radius - globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float rho = H * tex_coord_to_unit_range(uvwz.w, SCATTERING_TEXTURE_R_SIZE);
    float r = sqrt(rho * rho + globals.parameters.bottom_radius * globals.parameters.bottom_radius);
    float mu;
    bool ray_r_mu_intersects_ground;

    if (uvwz.z < 0.5) {
        float d_min = r - globals.parameters.bottom_radius;
        float d_max = rho;
        float d = d_min + (d_max - d_min) * tex_coord_to_unit_range(
            1.0 - 2.0 * uvwz.z, SCATTERING_TEXTURE_MU_SIZE / 2);
        mu = d == 0.0 ? -1.0 :
            clamp(-(rho * rho + d * d) / (2.0 * r * d), -1.0, 1.0);
        ray_r_mu_intersects_ground = true;
    } else {
        float d_min = globals.parameters.top_radius - r;
        float d_max = rho + H;
        float d = d_min + (d_max - d_min) * tex_coord_to_unit_range(
            2.0 * uvwz.z - 1.0, SCATTERING_TEXTURE_MU_SIZE / 2);
        mu = d == 0.0 ? 1.0 :
            clamp((H * H - rho * rho - d * d) / (2.0 * r * d), -1.0, 1.0);
        ray_r_mu_intersects_ground = false;
    }

    float x_mu_s = tex_coord_to_unit_range(uvwz.y, SCATTERING_TEXTURE_MU_S_SIZE);
    float d_min = globals.parameters.top_radius - globals.parameters.bottom_radius;
    float d_max = H;
    float A = -2.0 * globals.parameters.mu_s_min * globals.parameters.bottom_radius / (d_max - d_min);
    float a = (A - x_mu_s * A) / (1.0 + x_mu_s * A);
    float d = d_min + min(a, A) * (d_max - d_min);
    float mu_s = d == 0.0 ? 1.0 : clamp((H * H - d * d) / (2.0 * globals.parameters.bottom_radius * d), -1.0, 1.0);
    float nu = clamp(
        clamp(uvwz.x * 2.0 - 1.0, -1.0, 1.0),
        mu * mu_s - sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s)),
        mu * mu_s + sqrt((1.0 - mu * mu) * (1.0 - mu_s * mu_s)));

    const int SAMPLE_COUNT = 50;
    float dx;
    if (ray_r_mu_intersects_ground) {
        dx = max(-r * mu - sqrt(max(r * r * (mu * mu - 1.0) + globals.parameters.bottom_radius * globals.parameters.bottom_radius, 0.0)), 0.0) / SAMPLE_COUNT;
    } else {
        dx = max(-r * mu + sqrt(max(r * r * (mu * mu - 1.0) + globals.parameters.top_radius * globals.parameters.top_radius, 0.0)), 0.0) / SAMPLE_COUNT;
    }

    vec3 rayleigh_mie_sum = vec3(0.0);
    for (int i = 0; i <= SAMPLE_COUNT; i++) {
        float d_i = i * dx;
        float r_i = clamp(sqrt(d_i * d_i + 2.0 * r * mu * d_i + r * r), globals.parameters.bottom_radius, globals.parameters.top_radius);
        float mu_i = clamp((r * mu + d_i) / r_i, -1.0, 1.0);
        float mu_s_i = clamp((r * mu_s + d_i * nu) / r_i, -1.0, 1.0);
        vec3 rayleigh_mie_i =
            get_scattering(
                globals.parameters, scattering_density_texture, r_i, mu_i, mu_s_i, nu,
                ray_r_mu_intersects_ground) *
            get_transmittance(
                globals.parameters, transmittance_texture, r, mu, d_i,
                ray_r_mu_intersects_ground) *
            dx;
        float weight_i = (i == 0 || i == SAMPLE_COUNT) ? 0.5 : 1.0;
        rayleigh_mie_sum += rayleigh_mie_i * weight_i;
    }

    delta_multiple_scattering = rayleigh_mie_sum;
    scattering = vec4(
        globals.parameters.luminance_from_radiance * delta_multiple_scattering.rgb / rayleigh_phase_function(nu),
        0.0);
}
