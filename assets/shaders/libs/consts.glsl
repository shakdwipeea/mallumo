const float PI = 3.14159265358979323846f;
const float HALF_PI = 1.57079f;
const float EPSILON = 1e-30f;
const float SQRT_3 = 1.73205080f;