///
/// Fresnel-Schlick approximation
///
/// f0 - base reflectivity of the surface
/// N ⋅ V
///
vec3 fresnelSchlick(float NdotV, vec3 f0)
{
    return f0 + (1.0 - f0) * pow(1.0 - NdotV, 5.0);
}

vec3 cook_torrance_fresnel(float NdotV, vec3 f0)
{
    return fresnelSchlick(NdotV, f0);
}
