#version 450 core
#extension GL_ARB_shader_draw_parameters : require

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/vertices.glsl

layout(std140, binding = 0) uniform Globals 
{
  mat4 light_space_matrix;
};

struct PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  // Padding
  uint padding[27];                   // 148B padded to 256
};

layout(std430, binding = PRIMITIVE_PARAMETERS_BINDING) buffer PrimitiveParametersBuffer {
  PrimitiveParameters primitive_parameters_buffer[];
};

out vec3 world_position;

void main(void)
{
  const PrimitiveParameters primitive_parameters = primitive_parameters_buffer[gl_DrawIDARB];

  vec3 position = vertices[gl_VertexID].position.xyz;
  world_position = vec3(primitive_parameters.model_matrix * vec4(position, 1.0));

  gl_Position = light_space_matrix * vec4(world_position, 1.0);
}