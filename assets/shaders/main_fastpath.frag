#version 450 core
#extension GL_ARB_shader_draw_parameters : require
#extension GL_ARB_bindless_texture : require

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/consts.glsl

#include libs/vertices.glsl

#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl


layout(std140, binding = 0) uniform Globals {
    uint has_sun;
    uint has_point_lights;
} globals;

layout(std140, binding = 1) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

layout(std140, binding = 2) uniform Sun {
  mat4 space_matrix;
  vec4 color;
  vec4 position;
} sun;

struct PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  uint sampler_padding;

  // Bindless samplers 
  sampler2D albedo_sampler;            // 8B
  sampler2D normal_sampler;            // 8B

  // Padding
  uint padding[22];                   // 164B padded to 256
};

layout(std430, binding = PRIMITIVE_PARAMETERS_BINDING) buffer PrimitiveParametersBuffer {
  PrimitiveParameters primitive_parameters_buffer[];
};

in vertexOut {
  mat3 TBN;
  vec3 world_position; 
  vec2 texture_coordinate;
  vec3 normal;
  vec4 sun_space_position;
  flat int draw_id;
} vertex_out;

layout(binding = 6) uniform sampler2D sun_shadowmap;

layout(location = 0) out vec4 color;

float in_sun_shadow(vec4 sun_space_position, vec3 normal)
{
    // perform perspective divide
    vec3 projection_coords = sun_space_position.xyz / sun_space_position.w;

    // transform to [0,1] range
    projection_coords.xy = projection_coords.xy * 0.5 + 0.5;

    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closest_depth = texture(sun_shadowmap, projection_coords.xy).r; 

    // get depth of current fragment from light's perspective
    float current_depth = projection_coords.z;

    const int kernel_size = 6;
    const int samples_count = kernel_size * 2 + 1;

    float shadow = 0.0;
    vec2 texel_size = 1.0 / textureSize(sun_shadowmap, 0);
    for(int x = -kernel_size; x <= kernel_size; ++x)
    {
        for(int y = -kernel_size; y <= kernel_size; ++y)
        {
            float depth = texture(sun_shadowmap, projection_coords.xy + vec2(x, y) * texel_size).r;
            shadow += current_depth < depth ? 1.0 : 0.0;
        }    
    }
    shadow /= samples_count * samples_count;

    return shadow;
}

void main()
{
    const PrimitiveParameters primitive_parameters = primitive_parameters_buffer[vertex_out.draw_id];

    // Material values
    vec4 albedo = primitive_parameters.albedo.rgba;
    if (primitive_parameters.has_albedo_texture == 1) {
      albedo *= texture(primitive_parameters.albedo_sampler, vertex_out.texture_coordinate);
    }

    const float metalness = primitive_parameters.metallic_roughness_refraction.r;
    const float roughness = primitive_parameters.metallic_roughness_refraction.g;
    const float occlusion = 1.0;
    const vec3 normal = normalize(vertex_out.normal);
    const vec3 emission = primitive_parameters.emission.rgb;

    const vec3 N = normal;
    const vec3 V = normalize(vec3(camera.position) - vertex_out.world_position);
    const vec3 R = reflect(-V, N); 

    // Approximation of F0 for Fresnel
    const vec3 f0 = mix(vec3(0.04, 0.04, 0.04), albedo.rgb, metalness);

    vec3 Lo = vec3(0.0);
    if (globals.has_sun == 1) {
        float sun_shadow = in_sun_shadow(vertex_out.sun_space_position, N);

        vec3 L = normalize(sun.position.xyz); 
        vec3 H = normalize(V + L);

        Lo += (1.0 - sun_shadow) * irradiance(N, H, V, L, sun.color.rgb, albedo, roughness, metalness);
    }

    Lo = Lo + emission;

    // HDR -> LDR using Reinhard operator
    // float min_white = 1.5;
    // Lo = (Lo * (vec3(1.0) + Lo / vec3(min_white * min_white))) / (vec3(1.0) + Lo);
    
    // Gamma correction
    Lo = pow(Lo, vec3(1.0/2.2));

    color = vec4(Lo, 1.0);
}