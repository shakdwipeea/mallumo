#version 450 core
#extension GL_ARB_shader_draw_parameters : require
#extension GL_ARB_bindless_texture : require

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/vertices.glsl

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

struct PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  uint sampler_padding;                // 4B

  // Bindless samplers 
  sampler2D albedo_sampler;             // 8B
  sampler2D normal_sampler;             // 8B
  sampler2D metallic_roughness_sampler; // 8B
  sampler2D emissive_sampler;           // 8B

  // Padding
  uint padding[17];                   // 188B padded to 256
};

layout(std430, binding = PRIMITIVE_PARAMETERS_BINDING) buffer PrimitiveParametersBuffer {
  PrimitiveParameters primitive_parameters_buffer[];
};

out vertexOut {
  mat3 TBN;
  vec3 world_position; 
  vec2 texture_coordinate;
  vec3 normal;
  flat int draw_id;
} vertex_out;

void main(void)
{
  const PrimitiveParameters primitive_parameters = primitive_parameters_buffer[gl_DrawIDARB];
  vertex_out.draw_id = gl_DrawIDARB;

  vec3 position = vertices[gl_VertexID].position.xyz;
  vec3 normal = vertices[gl_VertexID].normal.xyz;
  vec2 texture_coordinate = vertices[gl_VertexID].texture_coordinate.xy;
  vec3 tangent = vertices[gl_VertexID].tangent.xyz;
  float sign = vertices[gl_VertexID].tangent.w;
  vec3 bitangent = cross(normal, tangent) * sign;

  mat4 model_matrix = primitive_parameters.model_matrix;
  mat3 normal_matrix = transpose(inverse(mat3(model_matrix)));

  vec3 T = normalize(normal_matrix * tangent);
  vec3 B = normalize(normal_matrix * bitangent);
  vec3 N = normalize(normal_matrix * normal);  
  mat3 TBN = mat3(T, B, N);

  vertex_out.TBN = TBN;
  vertex_out.world_position = vec3(model_matrix * vec4(position, 1.0));
  vertex_out.texture_coordinate = texture_coordinate;
  vertex_out.normal = normalize(normal_matrix * normal);

  gl_Position = camera.projection_view_matrix * vec4(vertex_out.world_position, 1.0f);
}