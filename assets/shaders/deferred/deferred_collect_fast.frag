#version 450 core
#extension GL_ARB_bindless_texture : require


#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/consts.glsl
#include libs/vertices.glsl

#include libs/cook_torrance_distribution.glsl
#include libs/cook_torrance_fresnel.glsl
#include libs/cook_torrance_geometry.glsl
#include libs/pbr.glsl

struct PrimitiveParameters {
  mat4 model_matrix;                   // 64B

  // Material
  vec4 albedo;                         // 16B
  vec4 metallic_roughness_refraction;  // 16B
  vec4 emission;                       // 16B

  // Material Textures
  uint has_albedo_texture;             // 4B
  uint has_metallic_roughness_texture; // 4B
  uint has_occlusion_texture;          // 4B
  uint has_normal_texture;             // 4B
  uint has_emissive_texture;           // 4B

  // Image based lighting
  uint has_diffuse_ibl;                // 4B
  uint has_specular_ibl;               // 4B

  // Shadows
  uint casts_shadows;                  // 4B
  uint receives_shadows;               // 4B

  uint sampler_padding;                // 4B

  // Bindless samplers 
  sampler2D albedo_sampler;             // 8B
  sampler2D normal_sampler;             // 8B
  sampler2D metallic_roughness_sampler; // 8B
  sampler2D emissive_sampler;           // 8B

  // Padding
  uint padding[17];                   // 188B padded to 256
};

layout(std430, binding = PRIMITIVE_PARAMETERS_BINDING) buffer PrimitiveParametersBuffer {
  PrimitiveParameters primitive_parameters_buffer[];
};

struct Light {
    vec4 pos_far;
    vec3 color;
    uint has_shadowmap;
};

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

in vertexOut {
  mat3 TBN;
  vec3 world_position;  
  vec2 texture_coordinate;
  vec3 normal;
  flat int draw_id;
} vertex_out;

layout (location = 0) out vec3 positions;
layout (location = 1) out vec4 albedos;
layout (location = 2) out vec3 orms;
layout (location = 3) out vec3 normals;
layout (location = 4) out vec3 emissions;

void main()
{
    const PrimitiveParameters primitive_parameters = primitive_parameters_buffer[vertex_out.draw_id];

    vec4 albedo = primitive_parameters.albedo.rgba;
    if (primitive_parameters.has_albedo_texture == 1) {
        albedo *= texture(primitive_parameters.albedo_sampler, vertex_out.texture_coordinate).rgba;
    }

    if (albedo.a < 1.0) {
        discard;
    }
    
    float roughness = primitive_parameters.metallic_roughness_refraction.g;
    float metalness = primitive_parameters.metallic_roughness_refraction.r;
    if (primitive_parameters.has_metallic_roughness_texture == 1) {
        metalness *= texture(primitive_parameters.metallic_roughness_sampler, vertex_out.texture_coordinate).b;
        roughness *= texture(primitive_parameters.metallic_roughness_sampler, vertex_out.texture_coordinate).g;
    }

    const float occlusion = 1.0;

    vec3 normal = vertex_out.normal;
    if (primitive_parameters.has_normal_texture == 1) {
        normal = texture(primitive_parameters.normal_sampler, vertex_out.texture_coordinate).rgb;
        normal = normalize(normal * 2.0 - 1.0);
        normal = vertex_out.TBN * normal; 
    }
    normal = normalize(normal);

    vec3 emission = primitive_parameters.emission.rgb;
    if (primitive_parameters.has_emissive_texture == 1) {
        emission *= texture(primitive_parameters.emissive_sampler, vertex_out.texture_coordinate).rgb;
    }

    positions = vertex_out.world_position;
    albedos = albedo;
    orms = vec3(occlusion, roughness, metalness);
    normals = normal;
    emissions = emission;
}