// version
// vertices
// parameters

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;

void main(void)
{
  int index = indices[gl_VertexID];

  vec3 position = vertices[index].position.xyz;
  
  gl_Position = camera.projection_view_matrix * vec4(vec3(primitive_parameters.model_matrix * vec4(position, 1.0)), 1.0f);
}